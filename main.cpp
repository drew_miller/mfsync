/*
 * Copyright 2014 Andrew William Miller
 */

#include <QtCore/QDebug>
#include <QtWidgets/QApplication>

#include <util/common.h>

#include "com/mfsync/Control.h"
#include "com/mfsync/DatabaseManager.h"
#include "com/mfsync/Stuff.h"
#include "com/mfsync/types/RemoteFileInfo.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    app.setOrganizationDomain("mfsync.com");
    app.setApplicationName("mfsync");
    com::mfsync::loadSslCertificates();
    com::mfsync::registerMetatypes();
    com::mfsync::Control control(app);
    control.start();
    int retval = app.exec();
    return retval;
}
