MfSync: File Synchronization

Keep files in sync with DropBox. Protect data by encrypting files before uploading them. Only you and people you choose to share a directory's encryption password with can access the file contents.

MfSync uses AES256-CBC for encryption and the standard OpenSSL salt header for encrypting files, so files encrypted with MfSync can also be decrypted using OpenSSL on the command line:
openssl enc -aes-256-cbc -d -in encrypted_file.txt -out decrypted_file.txt

This tool is not licensed for commercial use, and should not be used in production. Don't use MfSync as the sole backup of your data. If you have questions or comments about MfSync, please email the author Drew Miller at dmiller309@gmail.com.
