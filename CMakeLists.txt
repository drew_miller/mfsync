project(MfSync CXX)

set(CMAKE_SYSTEM_NAME Linux)

# Required cmake version for Ninja and Qt's find_package
cmake_minimum_required(VERSION 2.8.11)

# global needed variables
set(APPLICATION_NAME ${PROJECT_NAME})

set(APPLICATION_VERSION_MAJOR "0")
set(APPLICATION_VERSION_MINOR "126")
set(APPLICATION_VERSION_PATCH "0")

set(APPLICATION_VERSION "${APPLICATION_VERSION_MAJOR}.${APPLICATION_VERSION_MINOR}.${APPLICATION_VERSION_PATCH}")

add_definitions(-DAPPLICATION_VERSION="${APPLICATION_VERSION}")

if(${CMAKE_BUILD_TYPE} MATCHES "Release")
    add_definitions(-D_FORTIFY_SOURCE=2)
endif(${CMAKE_BUILD_TYPE} MATCHES "Release")

# disallow in-source build
string(COMPARE EQUAL "${CMAKE_SOURCE_DIR}" "${CMAKE_BINARY_DIR}" _insource)
if(_insource)
    file(REMOVE [CMakeCache.txt CMakeFiles])
    message(FATAL_ERROR "${PROJECT_NAME} requires an out of source build.")
endif(_insource)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
# Enable C++11
set(CMAKE_CXX_FLAGS "-std=c++11")

# List the source files (Not sure if this is necessary because of prev. line)
set(mfsync_SOURCES
    main.cpp
    com/mfsync/Control.cpp
    com/mfsync/DatabaseManager.cpp
    com/mfsync/DropboxAPI.cpp
    com/mfsync/Encryption.cpp
    com/mfsync/FileTransfer.cpp
    com/mfsync/Stuff.cpp
    com/mfsync/Syncer.cpp
    com/mfsync/types/LocalFileInfo.cpp
    com/mfsync/types/MappedPath.cpp
    com/mfsync/types/PathMappingEntry.cpp
    com/mfsync/types/RemoteFileInfo.cpp
    com/mfsync/ui/MainWindow.cpp
    util/BiMap.cpp
    util/FolderUtil.cpp
    util/StringUtil.cpp
)

# List the header files
set(mfsync_HEADERS
    com/mfsync/Control.h
    com/mfsync/DatabaseManager.h
    com/mfsync/DropboxAPI.h
    com/mfsync/Encryption.h
    com/mfsync/FileTransfer.h
    com/mfsync/Stuff.h
    com/mfsync/Syncer.h
    com/mfsync/types/LocalFileInfo.h
    com/mfsync/types/MappedPath.h
    com/mfsync/types/ParameterMappable.h
    com/mfsync/types/PathMappingEntry.h
    com/mfsync/types/RemoteFileInfo.h
    com/mfsync/ui/MainWindow.h
    util/common.h
    util/BiMap.h
    util/FolderUtil.h
    util/StringUtil.h
)

set(mfsync_FORMS
    com/mfsync/ui/MainWindow.ui
)

set(mfsync_RESOURCES
    com/mfsync/MfSyncResources.qrc
)


#find OpenSSL
find_package(OpenSSL REQUIRED)
include_directories(${OPENSSL_INCLUDE_DIR})

# Find the Qt libraries.
find_package(Qt5Concurrent REQUIRED)
find_package(Qt5Core REQUIRED)
find_package(Qt5Gui REQUIRED)
find_package(Qt5Network REQUIRED)
find_package(Qt5Sql REQUIRED)
find_package(Qt5Svg REQUIRED)
find_package(Qt5WebKit REQUIRED)
find_package(Qt5WebKitWidgets REQUIRED)
find_package(Qt5Widgets REQUIRED)

#qt5_wrap_cpp(mfsync_HEADERS_MOC ${mfsync_HEADERS})
qt5_wrap_ui(mfsync_FORMS_HEADERS ${mfsync_FORMS})
qt5_add_resources(mfsync_RCC_SRC ${mfsync_RESOURCES})

add_executable(mfsync
    ${mfsync_SOURCES}
    ${mfsync_HEADERS_MOC}
    ${mfsync_RCC_SRC}
    ${mfsync_FORMS_HEADERS}
)

qt5_use_modules(mfsync Concurrent Core Gui Network Sql Svg WebKit WebKitWidgets Widgets)

target_link_libraries(mfsync ${OPENSSL_LIBRARIES})

install(TARGETS mfsync
  RUNTIME DESTINATION bin
)

# CPack
SET(CPACK_GENERATOR "TGZ;RPM;DEB")
SET(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "amd64")
set(CPACK_DEBIAN_PACKAGE_DEPENDS "libqt5concurrent5, libqt5core5a, libqt5gui5, libqt5network5, libqt5sql5, libqt5sql5-sqlite, libqt5webkit5, libqt5widgets5, libqt5gui5, libssl1.0.0, libsqlite3-0")
set(CPACK_PACKAGE_NAME "mfsync")
set(CPACK_PACKAGE_VENDOR "Craven Unlimited")
set(CPACK_PACKAGE_CONTACT "Andrew Miller <dmiller309@gmail.com>")
set(CPACK_DEBIAN_PACKAGE_SECTION "Utilities")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Synchronize files with DropBox")
set(CPACK_PACKAGE_DESCRIPTION "Keep files in sync with DropBox. Protect data by encrypting files before uploading them. Only you and people you choose to share a directory's encryption password with can access the file contents.")
set(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_CURRENT_SOURCE_DIR}/ReadMe.txt")
set(CPACK_PACKAGE_ICON "${CMAKE_CURRENT_SOURCE_DIR}/com/mfsync/resources/icon.ico")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/License.html")
set(CPACK_RPM_PACKAGE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/License.html")
set(CPACK_PACKAGE_VERSION_MAJOR "${APPLICATION_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${APPLICATION_VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${APPLICATION_VERSION_PATCH}")
set(CPACK_SOURCE_STRIP_FILES true)
set(CPACK_PACKAGE_EXECUTABLES "mfsync" "M file sync")
set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -Wl,-z,relro")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -Wl,-z,relro")
include(CPack)
