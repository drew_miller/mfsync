/*
 * Copyright 2014 Andrew William Miller
 */

#include "util/BiMap.h"

template <class Left, class Right>
BiMap<Left, Right>::BiMap() {
}

template <class Left, class Right>
bool BiMap<Left, Right>::containsLeft(Left left) {
    return leftToRight_.contains(left);
}

template <class Left, class Right>
bool BiMap<Left, Right>::containsRight(Right right) {
    return rightToLeft_.contains(right);
}

template <class Left, class Right>
void BiMap<Left, Right>::insert(Left left, Right right) {
    removeLeft(left);
    removeRight(right);
    leftToRight_.insert(left, right);
    rightToLeft_.insert(right, left);
}

template <class Left, class Right>
bool BiMap<Left, Right>::empty() {
    return leftToRight_.empty();
}

template <class Left, class Right>
QList<Left> BiMap<Left, Right>::left() const {
    return leftToRight_.keys();
}

template <class Left, class Right>
Left BiMap<Left, Right>::left(Right right) {
    return rightToLeft_.value(right);
}

template <class Left, class Right>
QMap<Left, Right> BiMap<Left, Right>::leftToRight() const {
    return leftToRight_;
}

template <class Left, class Right>
void BiMap<Left, Right>::removeLeft(Left left) {
    if (leftToRight_.contains(left)) {
        rightToLeft_.remove(leftToRight_.take(left));
    }
}

template <class Left, class Right>
void BiMap<Left, Right>::removeRight(Right right) {
    if (rightToLeft_.contains(right)) {
        leftToRight_.remove(rightToLeft_.take(right));
    }
}

template <class Left, class Right>
QList<Right> BiMap<Left, Right>::right() const {
    return rightToLeft_.keys();
}

template <class Left, class Right>
Right BiMap<Left, Right>::right(Left left) {
    return leftToRight_.value(left);
}

template <class Left, class Right>
QMap<Right, Left> BiMap<Left, Right>::rightToLeft() const {
    return rightToLeft_;
}

template <class Left, class Right>
int BiMap<Left, Right>::size() const {
    return rightToLeft_.size();
}
