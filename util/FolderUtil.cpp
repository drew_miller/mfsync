/*
 * Copyright 2014 Andrew William Miller
 */

#include "util/FolderUtil.h"

#include <QtCore/QDir>
#include <QtCore/QFileInfo>

FolderUtil::FolderUtil() {
}

void FolderUtil::createPathToFile(
        QString file) {
    QFileInfo fileInfo(file);
    if (!fileInfo.dir().exists()) {
        QDir::root().mkpath(fileInfo.absolutePath());
    }
}
