/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef UTIL_COMMON_H_
#define UTIL_COMMON_H_

#ifdef QT_NO_DEBUG
#   define X_ASSERT(x) (x)
#else
#   define X_ASSERT(x) Q_ASSERT(x)
#endif

#endif  // UTIL_COMMON_H_
