/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef UTIL_STRINGUTIL_H_
#define UTIL_STRINGUTIL_H_

#include <util/common.h>

#include <QtCore/QMap>
#include <QtCore/QSet>
#include <QtCore/QStandardPaths>
#include <QtCore/QString>

#include "util/BiMap.h"

namespace com {
namespace mfsync {

class StringUtil {
 public:
    static BiMap<QStandardPaths::StandardLocation, QString>
    typeToRemotePath_bimap();
    static QString implode(QSet<QString> values, QString glue = "");
    static bool isValidEmail(QString email);
    static bool isValidPassword(QString password);
    static QString oneZero(bool trueFalse);
    static QString randomPassword(int length);
};

}  // namespace mfsync
}  // namespace com

#endif  // UTIL_STRINGUTIL_H_
