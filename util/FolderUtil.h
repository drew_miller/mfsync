/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef UTIL_FOLDERUTIL_H_
#define UTIL_FOLDERUTIL_H_

#include <QtCore/QString>

class FolderUtil {
 public:
    FolderUtil();
    static void createPathToFile(QString file);
};

#endif  // UTIL_FOLDERUTIL_H_
