/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef UTIL_BIMAP_H_
#define UTIL_BIMAP_H_

#include <QtCore/QMap>
#include <QtCore/QObject>

#include "util/common.h"

template <class Left, class Right>
class BiMap {
 public:
    explicit BiMap();

    bool containsLeft(Left key);
    bool containsRight(Right value);
    bool empty();
    QList<Left> left() const;
    Left left(Right right);
    QMap<Left, Right> leftToRight() const;
    void insert(Left left, Right right);
    void removeLeft(Left key);
    void removeRight(Right value);
    QList<Right> right() const;
    Right right(Left left);
    QMap<Right, Left> rightToLeft() const;
    int size() const;

 private:
    QMap<Left, Right> leftToRight_;
    QMap<Right, Left> rightToLeft_;
};

#endif  // UTIL_BIMAP_H_
