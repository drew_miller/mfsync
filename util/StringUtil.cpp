/*
 * Copyright 2014 Andrew William Miller
 */

#include "util/StringUtil.h"

#include <QtCore/QObject>
#include <QtCore/QRegExp>
#include <QtCore/QSetIterator>

namespace com {
namespace mfsync {

QString StringUtil::implode(QSet<QString> values, QString glue) {
    QString imploded;
    QSetIterator<QString> it(values);
    if (it.hasNext()) {
        imploded += it.next();
        it.next();
    }
    for (; it.hasNext(); ) {
        imploded += glue;
        imploded += it.next();
    }
    return imploded;
}

bool StringUtil::isValidEmail(QString email) {
    QRegExp rx("[\\w\\d._%+-]+@[\\w\\d.-]+\\.[\\w]{2,4}");
    return rx.exactMatch(email);
}

bool StringUtil::isValidPassword(QString password) {
    QRegExp rx("[\\w\\d][\\w\\d~!@#$%^&*()_+<>?]{4,28}[\\w\\d]");
    return rx.exactMatch(password);
}

QString StringUtil::oneZero(bool trueFalse) {
    if (trueFalse) {
        return "1";
    } else {
        return "0";
    }
}

}  // namespace mfsync
}  // namespace com
