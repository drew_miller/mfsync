/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef COM_MFSYNC_CONTROL_H_
#define COM_MFSYNC_CONTROL_H_

#include <util/common.h>

#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QString>
#include <QtCore/QThread>
#include <QtWidgets/QApplication>

#define MF_CLIENT_ID "8xemc1rzi0oahav"
#define MF_REDIRECT_URI "https://www.mfsync.com/dropbox_oauth/"
#define SETTINGE_VALUE_FIRST_RUN "is_firstrun"

namespace com {
namespace mfsync {

class DropboxAPI;
class MainWindow;
class Syncer;

class Control : public QObject {
    Q_OBJECT

 public:
    explicit Control(const QApplication &app);
    ~Control();

    void start();
 public slots:
    void shutdown();

 public:
    QSettings *settings;
    DropboxAPI *api;
    MainWindow *mainWindow;
    const QApplication *app;
    QString sessionToken;
    Syncer *syncer;
    QThread syncerThread;
    bool isEnding;
};

}  // namespace mfsync
}  // namespace com

#endif  // COM_MFSYNC_CONTROL_H_
