/*
 * Copyright 2014 Andrew William Miller
 */

#include "com/mfsync/Encryption.h"

#include <math.h>
#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <stdlib.h>
#include <QtConcurrent/QtConcurrentRun>
#include <QtCore/QtGlobal>
#include <QtCore/QCryptographicHash>
#include <QtCore/QDebug>
#include <QtCore/QFutureWatcher>

namespace com {
namespace mfsync {

Encryption::Encryption() :
    QObject(0) {
}

Encryption::~Encryption() {
}

bool Encryption::encrypt(
        QIODevice *input,
        QIODevice *output,
        QString password,
        bool *cancel) {
    if (cancel != nullptr && *cancel) {
        return false;
    }
    if (password.isEmpty()) {
        bool result = copyFile(input, output, cancel);
        return result;
    }
    // OpenSSL-compatible 16-byte header with salt
    if (8 != output->write("Salted__")) {
        qDebug() << "Failed to write header 'Salted__' to file";
        return false;
    }
    unsigned char salt[8];
    if (!RAND_bytes(salt, 8)) {
        qDebug() << "Failed generating random salt";
        return false;
    }
    if (8 != output->write(reinterpret_cast<char*>(&salt[0]), 8)) {
        qDebug() << "Failed writing salt to file";
        return false;
    }
    // Generate key and input vector from password and salt
    unsigned char key[EVP_MAX_KEY_LENGTH], inputVector[EVP_MAX_IV_LENGTH];
    if (32 != EVP_BytesToKey(EVP_aes_256_cbc(), EVP_md5(), salt,
                            (unsigned char *) password.toUtf8().data(),
                            password.length(), 1, key, inputVector)) {
        qDebug() << "Failed generating key/iv from password and salt";
        return false;
    }
    // Initialize
    EVP_CIPHER_CTX context;
    EVP_CIPHER_CTX_init(&context);
    if (!EVP_EncryptInit_ex(&context, EVP_aes_256_cbc(), NULL, key,
                            inputVector)) {
        qDebug() << "Failed to initialize encryption context";
        return false;
    }
    // Update and encrypt
    char inData[AES_BLOCK_SIZE], outData[AES_BLOCK_SIZE];
    int outLength  = 0;
    while (true) {
        qint64 bytes_read = input->read(inData, AES_BLOCK_SIZE);
        if (bytes_read < 0) {
            return false;
        }
        if (!EVP_EncryptUpdate(&context, (unsigned char*)outData, &outLength,
                              (unsigned char*)inData, bytes_read)) {
            qDebug() << "Failed EVP_EncryptUpdate";
            return false;
        }
        if (output->write(outData, outLength) < 0) {
            qDebug() << "Failed writing EVP_EncryptUpdate data to file";
            return false;
        }
        if (bytes_read < AES_BLOCK_SIZE) {
            break;
        }
        if (cancel != nullptr && *cancel) {
            qDebug() << "Encryption cancelled because program is ending.";
            break;
        }
    }
    // Finalize
    if (!EVP_EncryptFinal(&context, (unsigned char*)outData, &outLength)) {
        qDebug() << "Failed to finalize encryption";
        return false;
    }
    if (!output->write(outData, outLength)) {
        qDebug() << "Failed to write encryption final to file";
        return false;
    }
    // Clean up and return
    EVP_CIPHER_CTX_cleanup(&context);
    return !(cancel != nullptr && *cancel);
}

bool Encryption::decrypt(
        QIODevice *input,
        QIODevice *output,
        QString password,
        bool *cancel) {
    if (cancel != nullptr && *cancel) {
        return false;
    }
    if (password.isEmpty()) {
        bool result = copyFile(input, output, cancel);
        return result;
    }
    if (password.isEmpty()) {
        char inData[AES_BLOCK_SIZE];
        while (true) {
            if (cancel != nullptr && *cancel) {
                return false;
            }
            qint64 bytes_read = input->read(inData, AES_BLOCK_SIZE);
            if (bytes_read < 0) {
                return false;
            }
            if (output->write(inData, bytes_read) < 0) {
                return false;
            }
            if (bytes_read < AES_BLOCK_SIZE) {
                return true;
            }
        }
    }
    // OpenSSL-compatible 16-byte header with salt
    if (QString("Salted__") != QString(input->read(8))) {
        return false;
    }
    char salt[8];
    if (8 != input->read(salt, 8)) {
        return false;
    }
    // Generate key and input vector from password and salt
    unsigned char key[EVP_MAX_KEY_LENGTH], inputVector[EVP_MAX_IV_LENGTH];
    if (32 != EVP_BytesToKey(
                EVP_aes_256_cbc(),
                EVP_md5(),
                (unsigned char *) &salt[0],
                (const unsigned char *) password.toUtf8().constData(),
                password.length(), 1, key, inputVector)) {
        return false;
    }
    // Initialize
    EVP_CIPHER_CTX context;
    EVP_CIPHER_CTX_init(&context);
    if (!EVP_DecryptInit_ex(&context, EVP_aes_256_cbc(), NULL, key,
                            inputVector)) {
        return false;
    }
    // Update and encrypt
    char inData[AES_BLOCK_SIZE], outData[AES_BLOCK_SIZE];
    int outLength  = 0;
    while (true) {
        qint64 bytes_read = input->read(inData, AES_BLOCK_SIZE);
        if (bytes_read < 0) {
            return false;
        }
        if (!EVP_DecryptUpdate(&context, (unsigned char*)outData, &outLength,
                              (unsigned char*)inData, bytes_read)) {
            return false;
        }
        if (output->write(outData, outLength) < 0) {
            return false;
        }
        if (bytes_read < AES_BLOCK_SIZE) {
            break;
        }
        if (cancel != nullptr && *cancel) {
            break;
        }
    }
    // Finalize
    if (!EVP_DecryptFinal(&context, (unsigned char*)outData, &outLength)) {
        return false;
    }
    output->write(outData, outLength);
    // Clean up and return
    EVP_CIPHER_CTX_cleanup(&context);
    return !(cancel != nullptr && *cancel);
}

bool Encryption::hash(
        QIODevice *input,
        QByteArray *result,
        bool *cancel) {
    if (cancel != nullptr && *cancel) {
        return false;
    }
    QCryptographicHash crypto(QCryptographicHash::Sha256);
    while (!input->atEnd() && !(cancel != nullptr && *cancel)) {
        crypto.addData(input->read(AES_BLOCK_SIZE));
    }
    if (cancel != nullptr && *cancel) {
        return false;
    }
    result->clear();
    result->append(crypto.result());
    return true;
}

bool Encryption::copyFile(
        QIODevice *input,
        QIODevice *output,
        bool *cancel) {
    char data[AES_BLOCK_SIZE];
    while (!(cancel != nullptr && *cancel)) {
        qint64 bytesRead = input->read(data, AES_BLOCK_SIZE);
        if (bytesRead == 0) {
            return true;
        } else if (bytesRead < 0) {
            return false;
        }
        if (bytesRead != output->write(data, bytesRead)) {
            return false;
        }
    }
    return false;
}

}  // namespace mfsync
}  // namespace com
