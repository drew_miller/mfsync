/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef COM_MFSYNC_STUFF_H_
#define COM_MFSYNC_STUFF_H_

namespace com {
namespace mfsync {

void loadSslCertificates();
void registerMetatypes();

}
}

#endif  // COM_MFSYNC_STUFF_H_
