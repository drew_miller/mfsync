/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef COM_MFSYNC_FILETRANSFER_H_
#define COM_MFSYNC_FILETRANSFER_H_

#include <util/common.h>

#include <QtCore/QFuture>
#include <QtCore/QFutureWatcher>
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QTemporaryFile>
#include <QtCore/QVariantMap>

#include "com/mfsync/DropboxAPI.h"

namespace com {
namespace mfsync {

class Syncer;

class FileTransfer : public QObject {
    Q_OBJECT

 public:
    explicit FileTransfer(
            Syncer *syncer,
            DropboxAPI *api,
            QObject *parent = 0);

// Download file
 public slots:
    void downloadFile(
            QString remotePath,
            QString localPath,
            QString password);

 private:
    struct DownloadContext {
        QString remotePath;
        QString localPath;
        QString encryptionPassword;
        QVariantMap metadata;
        QFutureWatcher<bool>* watcher;
        QFuture<bool> future;
        QFile *encryptedFile;
        QTemporaryFile *decryptedFile;
        QByteArray *fileHash;
    };
    QMap<QFile*, DownloadContext*> downloadTempFileToContext;

 private slots:
    void handle_fileDownloaded(
            QString remotePath,
            QFile *localFile,
            QVariantMap metadata);

 private:
    QMap<void*, DownloadContext*> downloadDecryptWatcherToContext;

 private slots:
    void handle_downloadDecryptionFinished();

 private:
    QMap<void*, DownloadContext*> downloadHashWatcherToContext;

 private slots:
    void handle_downloadHashFinished();

 signals:
    void signal_downloadComplete(
            QString remotePath,
            QString localPath,
            QString encryptionPassword,
            QVariantMap metadata);
    void signal_localFileCreated(
            QString localPath);

// Upload file
 public slots:
    void uploadFile(
            QString localPath,
            QString remotePath,
            QString encryptionPassword);

 private:
    struct UploadContext {
        QString localPath;
        QString remotePath;
        QString encryptionPassword;
        QFile *localFile;
        QTemporaryFile *encryptedFile;
        QFutureWatcher<bool>* watcher;
        QFuture<bool> future;
        QVariantMap metadata;
    };
    QMap<void*, UploadContext*> uploadEncryptionWatcherToContext;

 private slots:
    void handle_uploadEncryptionFinished();

 private:
    QMap<QFile*, UploadContext*> uploadLocalTempToContext;

 private slots:
    void handle_fileUploadComplete(
            QFile *localFile,
            QString remotePath,
            QVariantMap metadata);

 signals:
    void signal_fileUploadComplete(
            QString localPath,
            QString remotePath,
            QVariantMap metadata);

// Download folder
 public slots:
    void downloadFolder(
            QString localPath);

 private:
    Syncer *syncer;
    DropboxAPI *api;
};

}  // namespace mfsync
}  // namespace com

#endif  // COM_MFSYNC_FILETRANSFER_H_
