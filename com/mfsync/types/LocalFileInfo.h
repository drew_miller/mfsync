/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef COM_MFSYNC_TYPES_LOCALFILEINFO_H_
#define COM_MFSYNC_TYPES_LOCALFILEINFO_H_

#include <util/common.h>

#include <QtCore/QMetaType>
#include <QtCore/QString>
#include <QtCore/QDateTime>

#include "com/mfsync/types/MappedPath.h"

namespace com {
namespace mfsync {

struct LocalFileInfo {
    LocalFileInfo() :
        path(),
        exists(false),
        isDir(false),
        lastModified(0),
        fileHash("")
    {}

    bool operator==(const LocalFileInfo& rhs);
    bool operator!=(const LocalFileInfo& rhs);

    MappedPath path;
    bool exists;
    bool isDir;
    qint64 lastModified;
    QString fileHash;
};

}  // namespace mfsync
}  // namespace com

Q_DECLARE_METATYPE(com::mfsync::LocalFileInfo)

#endif  // COM_MFSYNC_TYPES_LOCALFILEINFO_H_
