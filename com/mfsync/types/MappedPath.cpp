/*
 * Copyright 2014 Andrew William Miller
 */

#include "com/mfsync/types/MappedPath.h"

namespace com {
namespace mfsync {

MappedPath::MappedPath(int mappingId, QString relativePath) {
    this->first = mappingId;
    this->second = relativePath;
}

MappedPath::MappedPath() {
    this->first = -1;
}

void MappedPath::setMapping(int pathMapping) {
    this->first = pathMapping;
}

int MappedPath::getMapping() {
    return this->first;
}

void MappedPath::setRelativePath(QString path) {
    this->second = path;
}

QString MappedPath::getRelativePath() {
    return this->second;
}

}  // namespace mfsync
}  // namespace com
