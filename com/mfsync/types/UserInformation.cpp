#include "UserInformation.h"
#include "../../../util/StringUtil.h"

namespace com {
namespace mfsync {

UserInformation::UserInformation(QObject *parent) :
    QObject(parent)
{
}

// birth_date : The user's birth date. It should take the format "yyyy-mm-dd".
QDate UserInformation::getBirthDate() {
    return birthDate;
}

void UserInformation::setBirthDate(QDate birthDate) {
    this->birthDate = birthDate;
}

// display_name : The user's display name.
QString UserInformation::getDisplayName() {
    return displayName;
}

void UserInformation::setDisplayName(QString displayName) {
    this->displayName = displayName;
}

// first_name : The user's first name.
QString UserInformation::getFirstName() {
    return firstName;
}

void UserInformation::setFirstName(QString firstName) {
    this->firstName = firstName;
}

// gender : The user's gender ('male', 'female', or 'none').
UserInformation::Gender UserInformation::getGender() {
    return gender;
}

void UserInformation::setGender(Gender gender) {
    this->gender = gender;
}

// last_name : The user's last name.
QString UserInformation::getLastName() {
    return lastName;
}

void UserInformation::setLastName(QString lastName) {
    this->lastName = lastName;
}

// location : The user's address location.
// Heck if I know what this is...
QString UserInformation::getLocation() {
    return location;
}

void UserInformation::setLocation(QString location) {
    this->location = location;
}

//primary_usage : The user's primary usage of this MediaFire account ('home', 'work', 'school', or 'none').
UserInformation::UsageType UserInformation::getPrimaryUsageType() {
    return primaryUsageType;
}

void UserInformation::setPrimaryUsageType(UsageType primaryUsageType) {
    this->primaryUsageType = primaryUsageType;
}

//newsletter : Whether to receive MediaFire site news via email ('yes' or 'no').
bool UserInformation::getReceivesNewsletter() {
    return receivesNewsletter;
}

void UserInformation::setReceivesNewsletter(bool receivesNewsletter) {
    this->receivesNewsletter = receivesNewsletter;
}

// subdomain : The user's subdomain.
QString UserInformation::getSubdomain() {
    return subdomain;
}

void UserInformation::setSubdomain(QString subdomain) {
    this->subdomain = subdomain;
}

// website : The user's website URL.
QString UserInformation::getWebsite() {
    return website;
}

void UserInformation::setWebsite(QString website) {
    this->website = website;
}

QString UserInformation::genderToString(UserInformation::Gender gender) {
    if(gender == GENDER_MALE) {
        return "male";
    } else if(gender == GENDER_FEMALE) {
        return "female";
    } else {
        return "none";
    }
}

QString UserInformation::usageTypeToString(UserInformation::UsageType usageType) {
    if(usageType == USAGE_TYPE_HOME) {
        return "home";
    } else if(usageType == USAGE_TYPE_SCHOOL) {
        return "school";
    } else if(usageType == USAGE_TYPE_WORK) {
        return "work";
    } else {
        return "none";
    }
}

QMap<QString, QString> UserInformation::toParameterMap() {
    QMap<QString, QString> map;
    map["birth_date"] = getBirthDate().toString("yyyy-MM-dd");
    map["display_name"] = getDisplayName();
    map["first_name"] = getFirstName();
    map["gender"] = genderToString(getGender());
    map["last_name"] = getLastName();
    map["location"] = getLocation();
    map["newsletter"] = StringUtil::yesNo(getReceivesNewsletter());
    map["primary_usage"] = usageTypeToString(getPrimaryUsageType());
    map["subdomain"] = getSubdomain();
    map["website"] = getWebsite();
    return map;
}

}
}
