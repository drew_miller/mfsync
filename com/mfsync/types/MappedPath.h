/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef COM_MFSYNC_TYPES_MAPPEDPATH_H_
#define COM_MFSYNC_TYPES_MAPPEDPATH_H_

#include <util/common.h>

#include <QtCore/QPair>
#include <QtCore/QString>

namespace com {
namespace mfsync {

struct MappedPath : public QPair<int, QString> {
    MappedPath(int mappingId, QString relativePath);
    MappedPath();
    void setMapping(int pathMapping);
    int getMapping();
    void setRelativePath(QString path);
    QString getRelativePath();
};

}  // namespace mfsync
}  // namespace com

#endif  // COM_MFSYNC_TYPES_MAPPEDPATH_H_
