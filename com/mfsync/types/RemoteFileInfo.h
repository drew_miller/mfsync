/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef COM_MFSYNC_TYPES_REMOTEFILEINFO_H_
#define COM_MFSYNC_TYPES_REMOTEFILEINFO_H_

#include <util/common.h>

#include <QtCore/QDateTime>
#include <QtCore/QMetaType>
#include <QtCore/QString>

#include "com/mfsync/types/MappedPath.h"

namespace com {
namespace mfsync {

struct RemoteFileInfo {
    RemoteFileInfo() :
        path(),
        exists(false),
        isDir(false),
        lastModified(0),
        revision(-1)
    {}

    bool operator==(const RemoteFileInfo& rhs);
    bool operator!=(const RemoteFileInfo& rhs);

    MappedPath path;
    bool exists;
    bool isDir;
    qint64 lastModified;
    QString revision;
};

}  // namespace mfsync
}  // namespace com

Q_DECLARE_METATYPE(com::mfsync::RemoteFileInfo)

#endif  // COM_MFSYNC_TYPES_REMOTEFILEINFO_H_
