/*
 * Copyright 2014 Andrew William Miller
 */

#include "com/mfsync/types/PathMappingEntry.h"

#include <QtCore/QHash>

namespace com {
namespace mfsync {

PathMappingEntry::PathMappingEntry() :
    id(0),
    localPath(""),
    remotePath(""),
    password("") {
}
PathMappingEntry::PathMappingEntry(
        int id,
        QString localPath,
        QString remotePath,
        QString password) :
    id(id),
    localPath(localPath),
    remotePath(remotePath),
    password(password) {
}
bool PathMappingEntry::operator==(const PathMappingEntry& rhs) const {
    return id == rhs.id &&
            localPath == rhs.localPath &&
            remotePath == rhs.remotePath &&
            password == rhs.password;
}
bool PathMappingEntry::operator!=(const PathMappingEntry& rhs) const {
    return !operator==(rhs);
}

}  // namespace mfsync
}  // namespace com

uint qHash(com::mfsync::PathMappingEntry entry, uint seed) {
    return qHash(entry.id + entry.localPath + entry.remotePath +
                 entry.password, seed);
}
