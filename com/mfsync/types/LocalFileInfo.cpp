/*
 * Copyright 2014 Andrew William Miller
 */

#include "com/mfsync/types/LocalFileInfo.h"

#include <QtCore/QDebug>

namespace com {
namespace mfsync {

bool LocalFileInfo::operator==(const LocalFileInfo& rhs) {
    return (path == rhs.path) &&
            (exists == rhs.exists) &&
            (isDir == rhs.isDir) &&
            (lastModified == rhs.lastModified) &&
            (fileHash == rhs.fileHash);
}

bool LocalFileInfo::operator!=(const LocalFileInfo& rhs) {
    return !operator==(rhs);
}

}  // namespace mfsync
}  // namespace com
