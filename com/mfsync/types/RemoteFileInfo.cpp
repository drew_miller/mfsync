/*
 * Copyright 2014 Andrew William Miller
 */

#include "com/mfsync/types/RemoteFileInfo.h"

#include <QtCore/QDebug>

namespace com {
namespace mfsync {

bool RemoteFileInfo::operator==(const RemoteFileInfo& rhs) {
    return (path == rhs.path) &&
            (exists == rhs.exists) &&
            (isDir == rhs.isDir) &&
            (lastModified == rhs.lastModified) &&
            (revision == rhs.revision);
}

bool RemoteFileInfo::operator!=(const RemoteFileInfo& rhs) {
    return !operator==(rhs);
}

}  // namespace mfsync
}  // namespace com
