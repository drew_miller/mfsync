#pragma once

#include <util/common.h>

#include "ParameterMappable.h"

#include <QtCore/QDate>
#include <QtCore/QObject>

namespace com {
namespace mfsync {

class UserInformation : public QObject, public IParameterMappable
{
    Q_OBJECT

public:
    explicit UserInformation(QObject *parent = 0);

    enum Gender {
        GENDER_NONE = 0,
        GENDER_MALE,
        GENDER_FEMALE
    };

    enum UsageType {
        USAGE_TYPE_NONE,
        USAGE_TYPE_HOME,
        USAGE_TYPE_WORK,
        USAGE_TYPE_SCHOOL
    };

    /**
     * @brief getBirthDate
     * @return The user's birth date.
     */
    QDate getBirthDate();

    /**
     * @brief getDisplayName
     * @return The user's display name.
     */
    QString getDisplayName();

    /**
     * @brief getFirstName
     * @return The user's first name.
     */
    QString getFirstName();

    /**
     * @brief getGender
     * @return The user's gender.
     */
    Gender getGender();

    /**
     * @brief getLastName
     * @return The user's last name.
     */
    QString getLastName();

    /**
     * @brief getLocation
     *          Heck if I know what this is...
     * @return The user's address location.
     */
    QString getLocation();

    /**
     * @brief getPrimaryUsageType
     * @return The user's primary usage of this MediaFire account.
     */
    UsageType getPrimaryUsageType();

    /**
     * @brief getReceivesNewsletter
     * @return Whether to receive MediaFire site news via email.
     */
    bool getReceivesNewsletter();

    /**
     * @brief getSubdomain
     * @return The user's subdomain.
     */
    QString getSubdomain();

    /**
     * @brief getWebsite
     * @return The user's website URL.
     */
    QString getWebsite();

    /**
     * @brief setBirthDate
     * @param birthDate
     *          The user's birth date.
     */
    void setBirthDate(QDate birthDate);

    /**
     * @brief setDisplayName
     * @param displayName
     *          The user's display name.
     */
    void setDisplayName(QString displayName);

    /**
     * @brief setGender
     * @param gender
     *          The user's gender.
     */
    void setGender(Gender gender);

    /**
     * @brief setFirstName
     * @param firstName
     *          The user's first name.
     */
    void setFirstName(QString firstName);

    /**
     * @brief setLastName
     * @param lastName
     *          The user's last name.
     */
    void setLastName(QString lastName);

    /**
     * @brief setLocation
     *          Heck if I know what this is...
     * @param location
     *          The user's address location.
     */
    void setLocation(QString location);

    /**
     * @brief setPrimaryUsageType
     * @param primaryUsageType
     *          The user's primary usage of this MediaFire account.
     */
    void setPrimaryUsageType(UsageType primaryUsageType);

    /**
     * @brief setReceivesNewsletter
     * @param receivesNewsletter
     *          Whether to receive MediaFire site news via email.
     */
    void setReceivesNewsletter(bool receivesNewsletter);

    /**
     * @brief setSubdomain
     * @param subdomain
     *          The user's subdomain.
     */
    void setSubdomain(QString subdomain);

    /**
     * @brief setWebsite
     * @param website
     *          The user's website URL.
     */
    void setWebsite(QString website);

    // Implement the IParameterMappable interface
    QMap<QString, QString> toParameterMap();

private:
    QString displayName;
    QString firstName;
    QString lastName;
    QDate birthDate;
    Gender gender;
    QString website;
    QString subdomain;
    QString location;
    bool receivesNewsletter;
    UsageType primaryUsageType;

    QString genderToString(Gender gender);
    QString usageTypeToString(UsageType usageType);

};

}
}
