/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef COM_MFSYNC_TYPES_PATHMAPPINGENTRY_H_
#define COM_MFSYNC_TYPES_PATHMAPPINGENTRY_H_

#include <QtCore/QMetaType>
#include <QtCore/QString>
#include <QtCore/QList>

namespace com {
namespace mfsync {

struct PathMappingEntry {
    PathMappingEntry();
    PathMappingEntry(int id, QString localpath, QString remotePath,
                     QString password);
    int id;
    QString localPath;
    QString remotePath;
    QString password;
    bool operator==(const PathMappingEntry& rhs) const;
    bool operator!=(const PathMappingEntry& rhs) const;
};

}  // namespace mfsync
}  // namespace com

uint qHash(com::mfsync::PathMappingEntry entry, uint seed = 0);

Q_DECLARE_METATYPE(com::mfsync::PathMappingEntry)

#endif  // COM_MFSYNC_TYPES_PATHMAPPINGENTRY_H_
