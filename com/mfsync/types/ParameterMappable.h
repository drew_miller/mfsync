#pragma once

#include <util/common.h>

#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QString>

namespace com {
namespace mfsync {

class IParameterMappable
{
public:
    virtual QMap<QString, QString> toParameterMap() = 0;
};

}
}
Q_DECLARE_INTERFACE(
        com::mfsync::IParameterMappable,
        "com::mfsync::IParameterMappable/1.0")
