/*
 * Copyright 2014 Andrew William Miller
 */

#include "com/mfsync/DropboxAPI.h"

#include <util/StringUtil.h>
#include <util/FolderUtil.h>

#include <QtCore/QByteArray>
#include <QtCore/QCryptographicHash>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtGlobal>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonValue>
#include <QtCore/QList>
#include <QtCore/QMapIterator>
#include <QtCore/QTimer>
#include <QtCore/QUrl>
#include <QtCore/QUrlQuery>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>

#include "com/mfsync/Encryption.h"

namespace com {
namespace mfsync {

DropboxAPI::DropboxAPI(
        QSettings *settings) :
    manager(new QNetworkAccessManager(this)),
    isAuthenticated_(true),
    isEnding(false),
    bearerToken(""),
    settings(settings) {
    deltaCursor = settings->value(SETTINGS_KEY_DELTA_CURSOR).toString();
    bearerToken = settings->value(SETTINGS_KEY_BEARER_TOKEN).toString();
}

DropboxAPI::~DropboxAPI() {
    /**
     * TODO: only save the delta cursor if the delta at the cursor has already
     * been processed.
     */
    settings->setValue(SETTINGS_KEY_DELTA_CURSOR, deltaCursor);
}

QString DropboxAPI::getBearerToken() {
    return bearerToken;
}

void DropboxAPI::setBearerToken(
        QString bearerToken) {
    if (this->bearerToken == bearerToken) {
        return;
    }
    this->bearerToken = bearerToken;
    settings->setValue(SETTINGS_KEY_BEARER_TOKEN, bearerToken);
    isAuthenticated_ = !bearerToken.isEmpty();
    deltaCursor.clear();
    if (isAuthenticated()) {
        watch();
    }
    signal_loginStatusChanged();
}

bool DropboxAPI::isAuthenticated() {
//    return isAuthenticated_ && !getBearerToken().isEmpty();
    return !getBearerToken().isEmpty();
}

void DropboxAPI::deAuthenticate() {
    if (!isAuthenticated()) {
        return;
    }
    QUrl url("https://api.dropbox.com/1/disable_access_token",
             QUrl::StrictMode);
    QNetworkRequest request(url);
    addBearerToken(request);
    QNetworkReply *deauthorizeReply = manager->get(request);
    X_ASSERT(connect(
                 deauthorizeReply, SIGNAL(finished()),
                 this, SLOT(handle_deAuthenticateFinished())));
}
void DropboxAPI::handle_deAuthenticateFinished() {
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    if (reply->error() == QNetworkReply::NoError) {
        setBearerToken("");
    }
    reply->deleteLater();
}

////////////////////////////////////////////////////////////////////////////
/// Filesystem watcher

void DropboxAPI::watch() {
    if (deltaMutex.tryLock()) {
        delta();
    }
}

void DropboxAPI::stop() {
    isEnding = true;
}

void DropboxAPI::delta() {
    if (!isAuthenticated() || isEnding) {
        deltaMutex.unlock();
        return;
    }
    QUrl url("https://api.dropbox.com/1/delta");
    QUrlQuery query;
    // TODO(dmiller309): Save the deltaCursor in QSettings.
    if (!deltaCursor.isEmpty()) {
        query.addQueryItem("cursor", deltaCursor);
    }
    url.setQuery(query);
    QNetworkRequest request(url);
    addBearerToken(request);
    request.setHeader(QNetworkRequest::ContentTypeHeader,
                      "application/x-www-form-urlencoded");
    QNetworkReply *deltaReply = manager->post(request, QByteArray());
    X_ASSERT(connect(
                 deltaReply, SIGNAL(finished()),
                 this, SLOT(handle_deltaReady())));
}

void DropboxAPI::handle_deltaReady() {
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    if (reply->error() == QNetworkReply::NoError) {
        QJsonDocument replyDocument = QJsonDocument::fromJson(reply->readAll());
        QJsonObject replyObject = replyDocument.object();
        QVariantMap replyMap = replyObject.toVariantMap();
        deltaCursor = replyMap.value("cursor").toString();
        if (deltaCursor.isEmpty()) {
            Q_ASSERT(!deltaCursor.isEmpty());
        }
        if (replyMap.value("reset", false).toBool()) {
            emit signal_deltaReset();
        }
        QVariantList metadataList;
        bool hasMore = replyMap.value("has_more", false).toBool();
        foreach (QVariant entry, replyMap.value("entries").toList()) {
            /**
             * TODO: Read all the metadata and send it to avoid having to make
             * another round trip.
             */
            QString modifiedPath = entry.toList()[0].toString();
            if (entry.toList()[1].isNull() || !entry.toList()[1].isValid()) {
                QVariantMap metadata;
                metadata["path"] = modifiedPath;
                metadata["is_deleted"] = true;
                metadataList.append(metadata);
            } else {
                metadataList.append(entry.toList()[1].toMap());
            }
        }
        emit signal_deltaReady(metadataList, hasMore);
        if (hasMore) {
            delta();
        } else {
            longPollDelta();
        }
    } else {
        qDebug() << "Error getting delta:" << reply->errorString();
        isAuthenticated_ = false;
        deltaMutex.unlock();
        emit signal_loginStatusChanged();
    }
    reply->deleteLater();
}

void DropboxAPI::longPollDelta() {
    if (!isAuthenticated() || isEnding) {
        deltaMutex.unlock();
        return;
    }
    if (deltaCursor.isEmpty()) {
        delta();
        return;
    }
    QUrl url("https://api-notify.dropbox.com/1/longpoll_delta");
    QUrlQuery query;
    query.addQueryItem("cursor", deltaCursor);
    url.setQuery(query);
    QNetworkRequest request(url);
    addBearerToken(request);
    QNetworkReply *longpollReply = manager->get(request);
    X_ASSERT(connect(
                 longpollReply, SIGNAL(finished()),
                 this, SLOT(handle_longpollDeltaReady())));
}

void DropboxAPI::handle_longpollDeltaReady() {
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    if (reply->error() == QNetworkReply::NoError) {
        QJsonDocument replyDocument = QJsonDocument::fromJson(reply->readAll());
        QJsonObject replyObject = replyDocument.object();
        if (replyObject.value("changes").toBool()) {
            delta();
        } else {
            if (replyObject.contains("backoff")) {
                qDebug() << "Received delta backoff:"
                         << replyObject["backoff"].toInt();
                QTimer::singleShot(replyObject["backoff"].toInt()*1000,
                        this, "longPollDelta");
            } else {
                longPollDelta();
            }
        }
    } else {
        qDebug() << "Error getting delta:" << reply->errorString();
        isAuthenticated_ = false;
        deltaMutex.unlock();
        emit signal_loginStatusChanged();
    }
    reply->deleteLater();
}

////////////////////////////////////////////////////////////////////////////
/// File metadata

void DropboxAPI::getMetadata(QString remotePath) {
    if (!isAuthenticated()) {
        return;
    }
    qDebug() << "getMetadata()" << remotePath;
    Q_ASSERT(remotePath.startsWith('/'));
    QUrl url("https://api.dropbox.com/1/metadata/dropbox" +
             escapePath(remotePath));
    QUrlQuery query;
    query.addQueryItem("list", "true");
    url.setQuery(query);
    QNetworkRequest request(url);
    addBearerToken(request);
    QNetworkReply *metadataReply = manager->get(request);
    metadataReplyToRemotePath[metadataReply] = remotePath;
    X_ASSERT(connect(
                 metadataReply, SIGNAL(finished()),
                 this, SLOT(handle_metadataReady())));
}

void DropboxAPI::handle_metadataReady() {
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    QString remotePath = metadataReplyToRemotePath.take(reply);
    QMap<QString, QVariant> replyMap;
    if (reply->error() == QNetworkReply::NoError) {
        QJsonDocument replyDocument = QJsonDocument::fromJson(reply->readAll());
        QJsonObject replyObject = replyDocument.object();
        replyMap = replyObject.toVariantMap();
        emit signal_metadataReady(remotePath, replyMap);
    } else if ((reply->error() == QNetworkReply::ContentNotFoundError)) {
        replyMap["is_deleted"] = true;
        emit signal_metadataReady(remotePath, replyMap);
    } else if (reply->error() == QNetworkReply::AuthenticationRequiredError) {
        setBearerToken("");
    } else {
        // TODO(dmiller309): Deal with not being on the network.
        // TODO(dmiller309): Emit that getting the metadata failed.
    }
    reply->deleteLater();
}

////////////////////////////////////////////////////////////////////////////
/// Transfer operations
/// Note: HTTP-error code translation can be found in the Qt source code at
/// src/network/access/qhttpthreaddelegate.cpp

void DropboxAPI::createFolder(QString remotePath) {
    QUrl url("https://api.dropbox.com/1/fileops/create_folder");
    QUrlQuery query;
    query.addQueryItem("root", "dropbox");
    query.addQueryItem("path", remotePath);
    url.setQuery(query);
    QNetworkRequest request(url);
    if (!addBearerToken(request)) {
        bearerToken.clear();
        emit signal_loginStatusChanged();
        return;
    }
    QNetworkReply *reply = manager->get(request);
    replyToCreateFolderPath[reply] = remotePath;
    X_ASSERT(connect(
                 reply, SIGNAL(finished()),
                 this, SLOT(handle_folderCreateFinished())));
}
void DropboxAPI::handle_folderCreateFinished() {
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    QString folderPath = replyToCreateFolderPath.take(reply);
    QMap<QString, QVariant> metadata;
    if (reply->error() == QNetworkReply::NoError) {
        metadata["conflict"] = false;
        emit signal_folderCreated(folderPath, metadata);
    } else if (reply->error() ==
              QNetworkReply::ContentOperationNotPermittedError) {
        // The folder already existed.
        /**
         * TODO(dmiller309): Check the error pass by reference when reading the
         * QJsonDocument.
         */
        QJsonDocument replyDocument = QJsonDocument::fromJson(reply->readAll());
        QJsonObject replyObject = replyDocument.object();
        metadata = replyObject.toVariantMap();
        metadata["conflict"] = true;
        emit signal_folderCreated(folderPath, metadata);
    } else {
        isAuthenticated_ = false;
        emit signal_loginStatusChanged();
    }
    reply->deleteLater();
}

void DropboxAPI::deleteFile(QString remotePath) {
    QUrl url("https://api.dropbox.com/1/fileops/delete");
    QUrlQuery query;
    query.addQueryItem("root", "dropbox");
    query.addQueryItem("path", remotePath);
    url.setQuery(query);
    QNetworkRequest request(url);
    if (!addBearerToken(request)) {
        bearerToken.clear();
        emit signal_loginStatusChanged();
        return;
    }
    request.setHeader(QNetworkRequest::ContentTypeHeader,
                      "application/x-www-form-urlencoded");
    QNetworkReply *reply = manager->post(request, QByteArray());
    replyToRemoveFilePath[reply] = remotePath;
    X_ASSERT(connect(reply, SIGNAL(finished()),
                     this, SLOT(handle_deleteFileFinished())));
}
void DropboxAPI::handle_deleteFileFinished() {
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    QString folderPath = replyToRemoveFilePath.take(reply);
    QMap<QString, QVariant> metadata;
    if (reply->error() == QNetworkReply::NoError) {
        emit signal_fileRemoved(folderPath, metadata);
    } else if (reply->error() == QNetworkReply::ContentNotFoundError) {
        // The folder already existed.
        /**
         * TODO: Check the error pass by reference when reading the
         * QJsonDocument.
         */
        QJsonDocument replyDocument = QJsonDocument::fromJson(reply->readAll());
        QJsonObject replyObject = replyDocument.object();
        metadata = replyObject.toVariantMap();
        metadata["not_found"] = true;
        emit signal_fileRemoved(folderPath, metadata);
    } else if (reply->error() == QNetworkReply::UnknownContentError) {
        // Probably a 406 from removing too many entries.
        Q_ASSERT(false);
    } else {
        isAuthenticated_ = false;
        emit signal_loginStatusChanged();
    }
    reply->deleteLater();
}

void DropboxAPI::uploadFile(
        QFile *localFile,
        QString remotePath) {
    uploadFile(localFile, remotePath, 0);
}
void DropboxAPI::uploadFile(
        QFile *localFile,
        QString remotePath,
        QString uploadId) {
    qDebug() << "uploadFile" << remotePath;
    if (isEnding) {
        return;
    }
    QUrlQuery query;
    QNetworkRequest request;
    if (!addBearerToken(request)) {
        bearerToken.clear();
        emit signal_loginStatusChanged();
        return;
    }
    if (localFile->size() <= UPLOAD_CHUNK_SIZE) {
        // Put the file in one go
        QUrl url("https://api-content.dropbox.com/1/files_put/dropbox" +
                 escapePath(remotePath));
        request.setUrl(url);
        QByteArray encryptedData = localFile->readAll();
        qDebug() << "Uploading" << localFile->fileName() << "put";
        QNetworkReply *reply = manager->put(request, encryptedData);
        replyToUploadingLocalFile[reply] = localFile;
        replyToUploadingRemotePath[reply] = remotePath;
        X_ASSERT(connect(reply, SIGNAL(finished()),
                         this, SLOT(handle_fileUploadCommitFinished())));
    } else if (localFile->atEnd()) {
        // The file has been uploaded, time to commit the file.
        if (!uploadId.isEmpty()) {
            query.addQueryItem("upload_id", uploadId);
        }
        QUrl url("https://api-content.dropbox.com/1/commit_chunked_upload/"
                 "dropbox" + escapePath(remotePath));
        url.setQuery(query);
        request.setUrl(url);
        qDebug() << "Uploading" << localFile->fileName() << "finalizing";
        request.setHeader(QNetworkRequest::ContentTypeHeader,
                          "application/x-www-form-urlencoded");
        QNetworkReply *reply = manager->post(request, QByteArray());
        replyToUploadingLocalFile[reply] = localFile;
        replyToUploadingRemotePath[reply] = remotePath;
        X_ASSERT(connect(reply, SIGNAL(finished()),
                         this, SLOT(handle_fileUploadCommitFinished())));
    } else {
        // Upload a chunk of the file
        double offset= localFile->pos();
        QByteArray fileData = localFile->read(UPLOAD_CHUNK_SIZE);
        QUrl url("https://api-content.dropbox.com/1/chunked_upload");
        if (offset > 0) {
            query.addQueryItem("offset", QString::number(offset, 'g', 20));
        }
        if (!uploadId.isEmpty()) {
            query.addQueryItem("upload_id", uploadId);
        }
        qDebug() << "Uploading" << localFile->fileName() << "chunk" << offset;
        url.setQuery(query);
        request.setUrl(url);
        QNetworkReply *reply = manager->put(request, fileData);
        replyToUploadingLocalFile[reply] = localFile;
        replyToUploadingRemotePath[reply] = remotePath;
        X_ASSERT(connect(
                     reply, SIGNAL(finished()),
                     this, SLOT(handle_chunkUploaded())));
    }
}

void DropboxAPI::handle_chunkUploaded() {
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    QFile *localFile = replyToUploadingLocalFile.value(reply);
    QString remotePath = replyToUploadingRemotePath.value(reply);
    if (reply->error() != QNetworkReply::NoError) {
        isAuthenticated_ = false;
        emit signal_loginStatusChanged();
    }
    QJsonDocument replyDocument = QJsonDocument::fromJson(reply->readAll());
    QJsonObject replyObject = replyDocument.object();
    QString uploadId = replyObject["upload_id"].toString();
    uploadFile(localFile, remotePath, uploadId);
    reply->deleteLater();
}
void DropboxAPI::handle_fileUploadCommitFinished() {
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    QFile *localFile = replyToUploadingLocalFile.take(reply);
    QString remotePath = replyToUploadingRemotePath.take(reply);
    if (reply->error() != QNetworkReply::NoError) {
        // Either logged out, or incorrect parameters to the API's function.
        isAuthenticated_ = false;
        emit signal_loginStatusChanged();
    }
    QJsonDocument replyDocument = QJsonDocument::fromJson(reply->readAll());
    QJsonObject replyObject = replyDocument.object();
    emit signal_fileUploadComplete(localFile, remotePath,
                                   replyObject.toVariantMap());
    reply->deleteLater();
}

void DropboxAPI::downloadFile(
        QString remotePath,
        QFile *localFile) {
    if (isEnding) {
        return;
    }
    QUrl url("https://api-content.dropbox.com/1/files/dropbox" +
             escapePath(remotePath));
    QNetworkRequest request(url);
    if (!addBearerToken(request)) {
        bearerToken.clear();
        emit signal_loginStatusChanged();
        return;
    }
    QNetworkReply *reply = manager->get(request);
    replyToDownloadLocalFile[reply] = localFile;
    replyToDownloadRemotePath[reply] = remotePath;
    X_ASSERT(connect(reply, SIGNAL(readyRead()),
                     this, SLOT(handle_fileDownloadReadReady())));
    X_ASSERT(connect(reply, SIGNAL(finished()),
                     this, SLOT(handle_fileDownloadFinished())));
}
void DropboxAPI::handle_fileDownloadReadReady() {
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    QFile *localFile = replyToDownloadLocalFile.value(reply);
    X_ASSERT(localFile->write(reply->readAll()) >= 0);
}
void DropboxAPI::handle_fileDownloadFinished() {
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    QFile *localFile = replyToDownloadLocalFile.take(reply);
    QString remotePath = replyToDownloadRemotePath.take(reply);
    QByteArray dropboxHeader = reply->rawHeader("x-dropbox-metadata");
    QJsonDocument metadataDocument = QJsonDocument::fromJson(dropboxHeader);
    QMap<QString, QVariant> metadata = metadataDocument.object().toVariantMap();
    reply->deleteLater();
    emit signal_fileDownloadComplete(remotePath, localFile, metadata);
}

////////////////////////////////////////////////////////////////////////////////
/// User methods ///////////////////////////////////////////////////////////////

void DropboxAPI::getAccountInfo() {
    QUrl url("https://api.dropbox.com/1/account/info");
    QNetworkRequest request(url);
    if (!addBearerToken(request)) {
        emit signal_accountInfoReady(QVariantMap());
        return;
    }
    QNetworkReply *accountInfoReply = manager->get(request);
    X_ASSERT(connect(
                 accountInfoReply, SIGNAL(finished()),
                 this, SLOT(handle_accountInfoReplyFinished())));
}
void DropboxAPI::handle_accountInfoReplyFinished() {
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    if (reply->error() != QNetworkReply::NoError) {
        isAuthenticated_ = false;
    }
    QJsonDocument replyDocument = QJsonDocument::fromJson(reply->readAll());
    QJsonObject replyObject = replyDocument.object();
    emit signal_accountInfoReady(replyObject.toVariantMap());
    reply->deleteLater();
}

////////////////////////////////////////////////////////////////////////////////
/// Private utility methods ////////////////////////////////////////////////////

QString DropboxAPI::escapePath(QString remotePath) {
    QUrl url(remotePath);
    return QString(url.toPercentEncoding(remotePath, "/", ""));
}

bool DropboxAPI::addBearerToken(QNetworkRequest &request) {
    QString bearerToken = getBearerToken();
    if (bearerToken.isEmpty()) {
        return false;
    }
    request.setRawHeader("Authorization",
                         ("Bearer " + bearerToken).toUtf8().constData());
    return true;
}

void DropboxAPI::addParametersToQuery(
        QUrlQuery query,
        QMap<QString, QString> parameters) {
    for (QMapIterator<QString, QString> parameters_it(parameters);
        parameters_it.hasNext();
        parameters_it.next()) {
        query.addQueryItem(parameters_it.key(), parameters_it.value());
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Slots //////////////////////////////////////////////////////////////////////

}  // namespace mfsync
}  // namespace com
