/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef COM_MFSYNC_DATABASEMANAGER_H_
#define COM_MFSYNC_DATABASEMANAGER_H_

#include <util/common.h>

#include <QtCore/QMap>
#include <QtCore/QMetaType>
#include <QtCore/QMutex>
#include <QtCore/QPair>
#include <QtCore/QObject>
#include <QtCore/QVariant>
#include <QtCore/QString>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

#include "types/MappedPath.h"
#include "types/LocalFileInfo.h"
#include "types/PathMappingEntry.h"
#include "types/RemoteFileInfo.h"

namespace com {
namespace mfsync {

enum FileStatus {
    Unknown = 0,
    Unverified = 1,
    Verifying = 2,
    Verified = 3,
    Syncing = 4,
    Synced = 5
};

struct FilePropertiesEntry {
    LocalFileInfo lastSync_local;
    LocalFileInfo current_local;
    FileStatus current_local_status;
    RemoteFileInfo lastSync_remote;
    RemoteFileInfo current_remote;
    FileStatus current_remote_status;
};

}  // namespace mfsync
}  // namespace com
Q_DECLARE_METATYPE(com::mfsync::FilePropertiesEntry)

namespace com {
namespace mfsync {

class DatabaseManager : public QObject {
    Q_OBJECT

 public:
    explicit DatabaseManager(
            QString name = "data",
            QObject *parent = 0);
    ~DatabaseManager();

    void init();
    void reset();

    // Path mappings
    QString getBasePath(
            int pathMappingKey,
            bool isRemote);
    int addPathMapping(
            QString localPath,
            QString remotePath,
            QString password);
    int removePathMapping(
            int mappingId);
    PathMappingEntry getPathMapping(int id);
    QList<PathMappingEntry> getPathMappings();

    // Stuff for MainWindow
    double getSyncedCount();
    double getIndexingCount();
    double getSyncingCount();

    // File status
    FileStatus getStatus(
            MappedPath path,
            bool isRemote);
    void setStatus(
            MappedPath path,
            FileStatus status,
            bool setLocal = true,
            bool setRemote = true);
    void setChildrenStatus(
            MappedPath path,
            FileStatus status,
            bool setLocal = true,
            bool setRemote = true,
            bool recursive = true);
    QList<MappedPath> getUnverified(
            bool isRemote,
            int limit);
    QList<MappedPath> getVerifiedUnsynced(
            int limit);
    QSet<MappedPath> getFilesWithStatus(
            FileStatus status,
            bool getLocal,
            bool getRemote,
            int limit);

    // File properties
    bool fileExists(
            MappedPath path,
            bool isRemote = false);
    QSet<MappedPath> folderContents(
            MappedPath path,
            bool isRemote = false);
    FilePropertiesEntry getFileProperties(
            MappedPath path);
    void setFileProperties(
            LocalFileInfo localFile,
            FileStatus status = Verified);
    void setFileProperties(
            RemoteFileInfo remoteFile,
            FileStatus status = Verified);
    void invalidateAllRemote();

 private:
    void bindQueryFileStatuses(
            QSqlQuery &query);
    QString getDatabasePath(
            QString name);
    void cleanDeletedFiles();
    void updateLastSynced();
    void updateSyncStatus();
    FilePropertiesEntry queryToFileProperties(
            const QSqlQuery &query);
    MappedPath queryToMappedPath(
            const QSqlQuery &query);
    QString parentPath(
            QString path);
    QString pathStartPattern(
            QString path);

    QString name;
    QSqlDatabase db;
};

}  // namespace mfsync
}  // namespace com

#endif  // COM_MFSYNC_DATABASEMANAGER_H_
