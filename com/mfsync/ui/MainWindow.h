/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef COM_MFSYNC_UI_MAINWINDOW_H_
#define COM_MFSYNC_UI_MAINWINDOW_H_

#include <util/common.h>
#include <util/BiMap.h>

#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QtCore/QSet>
#include <QtCore/QSignalMapper>
#include <QtCore/QStandardPaths>
#include <QtCore/QString>
#include <QtCore/QTimer>
#include <QtCore/QUrl>
#include <QtGui/QStandardItemModel>
#include <QtWebKitWidgets/QWebView>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>

#include "../types/PathMappingEntry.h"

#define SETTINGS_KEY_SHOW_ADVANCED_SYNC "show_advanced_sync"

namespace com {
namespace mfsync {

namespace Ui {
class MainWindow;
}
class Control;

class MainWindow : public QMainWindow {
    Q_OBJECT

 public:
    explicit MainWindow(Control *control, QWidget *parent = 0);
    ~MainWindow();

 private:
    Ui::MainWindow *ui;
    Control *control;
    QStandardItemModel *model;
    QMap<QCheckBox*, PathMappingEntry> syncFolderCheckboxTypeMap;
    QList<QStandardPaths::StandardLocation> syncTypeOptionsList;
    QSpacerItem *syncTabSpacerWidget;
    QFrame *authenticationFrame;
    QFrame *authenticationCancelFrame;
    QWebView *signInWebView;

    QMap<QString, QString> parseFragmentMap(QString fragments);
    QString formatBytes(double bytes);

 public slots:
    void init();
    void handle_syncStatusReady(
            double indexingCount,
            double syncingCount,
            double syncedCount);

 private:
    QTimer updateStatusTimer;

 private slots:
    // Account tab
    void handle_accountInfoReady(QVariantMap accountInfo);
    void handle_cancelOAuth();
    void hideOauth();
    void handle_signInWebViewUrlChanged(QUrl url);
    void handle_loginStatusChanged();
    void on_authenticationSignInButton_clicked();
    void on_authenticationSignOutButton_clicked();

    // Sync tab
    void handle_syncPathCheckboxClicked();
    void handle_syncPathsChanged(QList<PathMappingEntry> pathMappings);
    void on_showAdvancedSyncCheckbox_toggled(bool checked);
    void on_removeSyncRowButton_clicked();
    void on_addSyncRowButton_clicked();
    void on_pathMapTable_itemSelectionChanged();
    void on_advancedSyncCancelAddButton_clicked();
    void on_advancedSyncApplyAddPath_clicked();

    void on_advancedSyncLocalPathInput_selectionChanged();
    void on_advancedSyncLocalPathInput_textEdited(const QString &arg1);
    void on_advancedSyncRemotePathInput_textChanged(const QString &arg1);
    void updateAddAdvancedEnabled();
};

}  // namespace mfsync
}  // namespace com

#endif  // COM_MFSYNC_UI_MAINWINDOW_H_
