/*
 * Copyright 2014 Andrew William Miller
 */

#include "com/mfsync/ui/MainWindow.h"

#include <QtCore/QtAlgorithms>
#include <QtCore/QByteArray>
#include <QtCore/QDebug>
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QPair>
#include <QtCore/QStandardPaths>
#include <QtCore/QStringList>
#include <QtCore/QUrlQuery>
#include <QtGui/QDesktopServices>
#include <QtGui/QStandardItem>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QAbstractButton>
#include <QtWidgets/QPushButton>
#include <QtNetwork/QNetworkCookieJar>
#include <QtSvg>

#include "ui_MainWindow.h"
#include "com/mfsync/Control.h"
#include "com/mfsync/DropboxAPI.h"
#include "com/mfsync/Syncer.h"
#include "util/StringUtil.h"

namespace com {
namespace mfsync {

MainWindow::MainWindow(
        Control *control,
        QWidget *parent) :
    QMainWindow(parent),
    ui(new mfsync::Ui::MainWindow),
    control(control),
    model(new QStandardItemModel()),
    syncTabSpacerWidget(nullptr),
    authenticationCancelFrame(nullptr),
    authenticationFrame(nullptr),
    signInWebView(nullptr) {
    ui->setupUi(this);

    int row = 0, col = 0;
    ui->pathMapTable->horizontalHeader()->
            setSectionResizeMode(QHeaderView::Stretch);
    QString dropDownCheckboxStylesheet =
            "QCheckBox::indicator:unchecked {"
            "  image: url(:/com/mfsync/resources/right_arrow.svg);"
            "} "
            "QCheckBox::indicator:checked {"
            "  image: url(:/com/mfsync/resources/down_arrow.svg);"
            "} ";
    ui->showAdvancedSyncCheckbox->setStyleSheet(dropDownCheckboxStylesheet);
    bool showAdvancedSync =
            control->settings->value(SETTINGS_KEY_SHOW_ADVANCED_SYNC).toBool();
    ui->advancedSyncAddMappingContainer->setVisible(showAdvancedSync);
    ui->showAdvancedSyncCheckbox->setChecked(showAdvancedSync);
    foreach (PathMappingEntry pathMapping,
            control->syncer->getDefaultPathMappings()) {
        QCheckBox *syncPathCheckbox =
                new QCheckBox(QFileInfo(pathMapping.localPath).fileName());
        ui->pathSelectionCheckboxes->addWidget(syncPathCheckbox, row, col);
        X_ASSERT(connect(syncPathCheckbox, SIGNAL(clicked()),
                         this, SLOT(handle_syncPathCheckboxClicked())));
        syncFolderCheckboxTypeMap[syncPathCheckbox] = pathMapping;
        row += !(col = (col+1) % 2);
    }
    ui->advancedSyncAddMappingContainer->setVisible(false);
    ui->mfsyncVersion->setText(QString(APPLICATION_VERSION));
    ui->mfsyncUpdated->setText(QString(__DATE__) + " at " + QString(__TIME__));
}

MainWindow::~MainWindow() {
    delete ui;
}

/// Account tab

void MainWindow::handle_accountInfoReady(QVariantMap accountInfo) {
    QString displayName = "-";
    if (accountInfo.contains("display_name")) {
        displayName = accountInfo.value("display_name").toString();
    }
    ui->accountUserValue->setText(displayName);
    if (accountInfo.contains("team")) {
        QVariantMap teamInfo = accountInfo["team"].toMap();
        if (teamInfo.contains("name")) {
            ui->accountTeamValue->setText(teamInfo["name"].toString());
        } else {
            ui->accountTeamValue->setText("-");
        }
    } else {
        ui->accountTeamValue->setText("-");
    }
    QString countryInfo;
    if (accountInfo.contains("country")) {
        countryInfo = accountInfo.value("country").toString();
    } else {
        countryInfo = "-";
    }
    ui->accountCountryValue->setText(countryInfo);
    if (accountInfo.contains("quota_info")) {
        QVariantMap quotaInfo = accountInfo["quota_info"].toMap();
        bool ok = false;
        double used = quotaInfo.value("normal").toDouble(&ok);
        if (ok) {
            used += quotaInfo.value("shared").toDouble(&ok);
            if (ok) {
                double total = quotaInfo.value("quota").toDouble(&ok);
                if (ok) {
                    ui->accountStorageValue->setText(
                                formatBytes(total-used) +
                                tr(" free of ") +
                                formatBytes(total));
                }
            }
        }
    } else {
        ui->accountStorageValue->setText("-");
    }
}

void MainWindow::handle_cancelOAuth() {
    hideOauth();
    handle_loginStatusChanged();
}

void MainWindow::hideOauth() {
    if (authenticationFrame != nullptr) {
        authenticationFrame->setVisible(false);
    }
    ui->configurationTabs->setVisible(true);
    ui->centralWidget->update();
}

void MainWindow::handle_signInWebViewUrlChanged(QUrl url) {
    if (MF_REDIRECT_URI == url.scheme() + "://" + url.host() + url.path()) {
        QMap<QString, QString> fragmentMap = parseFragmentMap(url.fragment());
        if (fragmentMap.contains("error")) {
            handle_cancelOAuth();
        } else if (fragmentMap.contains("access_token")) {
            Q_ASSERT(fragmentMap.contains("token_type"));
            Q_ASSERT(fragmentMap.value("token_type") == "bearer");
            control->api->setBearerToken(fragmentMap.value("access_token"));
        } else {
            handle_cancelOAuth();
        }
    }
}

void MainWindow::handle_loginStatusChanged() {
    bool isAuthenticated = control->api->isAuthenticated();
    if (isAuthenticated) {
        hideOauth();
        control->api->getAccountInfo();
    } else {
        handle_accountInfoReady(QVariantMap());
    }
    ui->authenticationSignInLabel->setVisible(!isAuthenticated);
    ui->authenticationSignInButton->setVisible(!isAuthenticated);
    ui->authenticationSignOutLabel->setVisible(isAuthenticated);
    ui->authenticationSignOutButton->setVisible(isAuthenticated);
}

/// Sync tab

void MainWindow::init() {
    X_ASSERT(connect(
                 control->api, SIGNAL(signal_loginStatusChanged()),
                 this, SLOT(handle_loginStatusChanged()),
                 Qt::QueuedConnection));
    X_ASSERT(connect(
                 control->api, SIGNAL(signal_accountInfoReady(QVariantMap)),
                 this, SLOT(handle_accountInfoReady(QVariantMap)),
                 Qt::QueuedConnection));
    X_ASSERT(connect(
                 control->syncer,
                 SIGNAL(signal_syncPathsChanged(QList<PathMappingEntry>)),
                 this,
                 SLOT(handle_syncPathsChanged(QList<PathMappingEntry>)),
                 Qt::QueuedConnection));
    X_ASSERT(connect(
                 control->syncer,
                 SIGNAL(signal_syncStatusReady(double, double, double)),
                 this,
                 SLOT(handle_syncStatusReady(double, double, double)),
                 Qt::QueuedConnection));
    setWindowIcon(QIcon(":/com/mfsync/resources/icon.ico"));
    handle_syncPathsChanged(control->syncer->dbm->getPathMappings());
    on_showAdvancedSyncCheckbox_toggled(false);
    on_pathMapTable_itemSelectionChanged();
    handle_loginStatusChanged();
    control->api->getAccountInfo();
}

void MainWindow::handle_syncStatusReady(
        double indexingCount,
        double syncingCount,
        double syncedCount) {
    ui->filesSyncedLabel->setText(QString::number(syncedCount));
    ui->filesIndexingLabel->setText(QString::number(indexingCount));
    ui->filesSyncingLabel->setText(QString::number(syncingCount));
}

void MainWindow::handle_syncPathCheckboxClicked() {
    QCheckBox *clickedCheckbox = qobject_cast<QCheckBox *>(sender());
    PathMappingEntry pathMapping =
            syncFolderCheckboxTypeMap.value(clickedCheckbox);
    if (clickedCheckbox->isChecked()) {
        X_ASSERT(QMetaObject::invokeMethod(
                     control->syncer, "addSyncPathMapping",
                     Qt::QueuedConnection,
                     Q_ARG(QString, pathMapping.localPath),
                     Q_ARG(QString, pathMapping.remotePath),
                     Q_ARG(QString, "")));
        control->syncer->syncPathsModifiedExternal++;
    } else {
        X_ASSERT(QMetaObject::invokeMethod(
                     control->syncer, "removeSyncPathMapping",
                     Qt::QueuedConnection,
                     Q_ARG(QString, pathMapping.localPath),
                     Q_ARG(QString, pathMapping.remotePath)));
        control->syncer->syncPathsModifiedExternal++;
    }
}

void MainWindow::handle_syncPathsChanged(QList<PathMappingEntry> pathMappings) {
    foreach (QCheckBox *checkBox, syncFolderCheckboxTypeMap.keys()) {
        bool setChecked = false;
        PathMappingEntry checkboxPathMapping =
                syncFolderCheckboxTypeMap.value(checkBox);
        foreach (PathMappingEntry dbPathMapping, pathMappings) {
            if (QFileInfo(checkboxPathMapping.localPath).absoluteFilePath() ==
                    QFileInfo(dbPathMapping.localPath).absoluteFilePath() &&
                    (checkboxPathMapping.remotePath.toLower() ==
                     dbPathMapping.remotePath.toLower())) {
                setChecked = true;
                break;
            }
        }
        checkBox->setChecked(setChecked);
    }
    for (int i = ui->pathMapTable->rowCount()-1; i >= 0; i--) {
        ui->pathMapTable->removeRow(i);
    }
    ui->pathMapTable->setSortingEnabled(false);
    int row = 0;
    foreach (PathMappingEntry pathMapping,
            pathMappings) {
        ui->pathMapTable->insertRow(row);
        // It's okay to use "new" here without "delete" because pathMapTable
        // takes ownership of the QTableWidgetItems and deletes them later.
        QTableWidgetItem *localFileItem =
                new QTableWidgetItem(pathMapping.localPath);
        QTableWidgetItem *remoteFileItem =
                new QTableWidgetItem(pathMapping.remotePath);
        QTableWidgetItem *passwordItem =
                new QTableWidgetItem(pathMapping.password);
        localFileItem->setToolTip(pathMapping.localPath);
        remoteFileItem->setToolTip(pathMapping.remotePath);
        passwordItem->setToolTip(pathMapping.password);
        ui->pathMapTable->setItem(row, 0, localFileItem);
        ui->pathMapTable->setItem(row, 1, remoteFileItem);
        ui->pathMapTable->setItem(row, 2, passwordItem);
        row++;
    }
    ui->pathMapTable->sortByColumn(0, Qt::AscendingOrder);
    ui->pathMapTable->setSortingEnabled(true);
}

QMap<QString, QString> MainWindow::parseFragmentMap(QString fragments) {
    if (fragments.startsWith("#")) {
        fragments = fragments.right(fragments.length()-1);
    }
    QMap<QString, QString> fragmentMap;
    foreach (QString fragmentParamPair,
            fragments.split("&", QString::SplitBehavior::SkipEmptyParts)) {
        QString key, value;
        if (fragmentParamPair.contains("=")) {
            int splitIndex = fragmentParamPair.indexOf("=");
            key = fragmentParamPair.left(splitIndex);
            int rightIndex = fragmentParamPair.length() - (splitIndex + 1);
            value = fragmentParamPair.right(rightIndex);
        } else {
            key = fragmentParamPair;
            value = "";
        }
        fragmentMap[key] = value;
    }
    return fragmentMap;
}

QString MainWindow::formatBytes(double bytes) {
    QList<QString> powerSymbols;
    powerSymbols << "B" << "kB" << "MB" << "GB" << "TB" << "PB" << "EB" << "ZB"
                 << "YB";
    double d = 0;
    for (; d < powerSymbols.length(); d++) {
        if (bytes < qPow(1000, d+1)) {
            break;
        }
    }
    return QString::number(bytes / qPow(1000, d), 'g', 3) + powerSymbols[d];
}

////////////////////////////////////////////////////////////////////////////////
/// UI Handlers ////////////////////////////////////////////////////////////////

/// Account tab ////////////////////////////////////////////////////////////////

void MainWindow::on_removeSyncRowButton_clicked() {
    QList<PathMappingEntry> mappingsToRemove;
    foreach (QTableWidgetSelectionRange selectionRange,
            ui->pathMapTable->selectedRanges()) {
        for (int row = selectionRange.topRow();
             row < selectionRange.bottomRow()+1;
             row++) {
            PathMappingEntry pathMappingEntry;
            pathMappingEntry.localPath =
                    ui->pathMapTable->item(row, 0)->text();
            pathMappingEntry.remotePath =
                    ui->pathMapTable->item(row, 1)->text();
            pathMappingEntry.password =
                    ui->pathMapTable->item(row, 2)->text();
            mappingsToRemove.append(pathMappingEntry);
        }
    }
    control->syncer->syncPathsModifiedExternal++;
    foreach (PathMappingEntry mappingToRemove, mappingsToRemove) {
        qDebug() << "A0:" << mappingToRemove.id;
        X_ASSERT(QMetaObject::invokeMethod(
                     control->syncer, "removeSyncPathMapping",
                     Qt::BlockingQueuedConnection,
                     Q_ARG(QString, mappingToRemove.localPath),
                     Q_ARG(QString, mappingToRemove.remotePath)));
        qDebug() << "A:" << mappingToRemove.localPath
                 << mappingToRemove.remotePath;
    }
}

void MainWindow::on_addSyncRowButton_clicked() {
    ui->advancedSyncAddMappingContainer->setVisible(true);
    ui->pathMapTable->setEnabled(false);
    ui->advancedSyncRemoveAddContainer->setVisible(false);
    ui->removeSyncRowButton->setEnabled(true);
    updateAddAdvancedEnabled();
}

void MainWindow::on_advancedSyncCancelAddButton_clicked() {
    ui->advancedSyncAddMappingContainer->setVisible(false);
    ui->advancedSyncLocalPathInput->clear();
    ui->advancedSyncRemotePathInput->clear();
    ui->pathMapTable->setEnabled(true);
    ui->advancedSyncRemoveAddContainer->setVisible(true);
}

void MainWindow::on_pathMapTable_itemSelectionChanged() {
    ui->removeSyncRowButton->setEnabled(
                !ui->pathMapTable->selectedItems().isEmpty());
}

void MainWindow::on_advancedSyncApplyAddPath_clicked() {
    ui->pathMapTable->setEnabled(true);
    ui->advancedSyncAddMappingContainer->setVisible(false);
    PathMappingEntry mappingEntry;
    mappingEntry.localPath = ui->advancedSyncLocalPathInput->text();
    mappingEntry.remotePath = ui->advancedSyncRemotePathInput->text();
    mappingEntry.password = ui->advancedSyncPasswordInput->text();
    X_ASSERT(QMetaObject::invokeMethod(
                 control->syncer, "addSyncPathMapping", Qt::QueuedConnection,
                 Q_ARG(QString, mappingEntry.localPath),
                 Q_ARG(QString, mappingEntry.remotePath),
                 Q_ARG(QString, mappingEntry.password)));
    ui->advancedSyncRemoveAddContainer->setVisible(true);
}

void MainWindow::on_authenticationSignInButton_clicked() {
    ui->configurationTabs->setVisible(false);
    if (authenticationFrame == nullptr) {
        authenticationFrame = new QFrame();
        authenticationFrame->setLayout(new QVBoxLayout());
        // Allow user to cancel authentication
        authenticationCancelFrame = new QFrame();
        QLabel *authenticationLabel = new QLabel(
                    tr("Please allow mfsync to connect with your DropBox "
                       "account."));
        authenticationLabel->setSizePolicy(QSizePolicy::Expanding,
                                           QSizePolicy::MinimumExpanding);
        QLayout *authenticationCancelLayout = new QHBoxLayout();
        authenticationCancelLayout->addWidget(authenticationLabel);
        QPushButton *cancelAuthenticationButton = new QPushButton(tr("Cancel"));
        X_ASSERT(connect(
                     cancelAuthenticationButton, SIGNAL(clicked()),
                     this, SLOT(handle_cancelOAuth())));
        authenticationCancelLayout->addWidget(cancelAuthenticationButton);
        authenticationCancelFrame->setLayout(authenticationCancelLayout);
        authenticationFrame->layout()->addWidget(authenticationCancelFrame);
        // Authenticate in web browser
        ui->configurationTabs->setVisible(false);
        signInWebView = new QWebView();
        X_ASSERT(connect(
                     signInWebView, SIGNAL(urlChanged(QUrl)),
                     this, SLOT(handle_signInWebViewUrlChanged(QUrl))));
        ui->centralWidget->layout()->addWidget(authenticationFrame);
        authenticationFrame->setSizePolicy(QSizePolicy::Expanding,
                                           QSizePolicy::Expanding);
        authenticationFrame->layout()->addWidget(signInWebView);
    }
    signInWebView->page()->networkAccessManager()->setCookieJar(
                new QNetworkCookieJar());
    QUrl signInUrl("https://www.dropbox.com/1/oauth2/authorize");
    QUrlQuery query;
    query.addQueryItem("response_type", "token");
    query.addQueryItem("client_id", MF_CLIENT_ID);
    query.addQueryItem("redirect_uri", MF_REDIRECT_URI);
    signInUrl.setQuery(query);
    signInWebView->load(signInUrl);
    authenticationFrame->show();
}

void MainWindow::on_authenticationSignOutButton_clicked() {
    control->api->deAuthenticate();
}

/// Sync tab ///////////////////////////////////////////////////////////////////

void MainWindow::on_showAdvancedSyncCheckbox_toggled(bool checked) {
    bool showAdvancedSync = checked;
    control->settings->setValue(SETTINGS_KEY_SHOW_ADVANCED_SYNC,
                                showAdvancedSync);
    ui->advancedSyncContainer->setVisible(showAdvancedSync);
    if (syncTabSpacerWidget != nullptr) {
        ui->syncTab->layout()->removeItem(syncTabSpacerWidget);
    }
    if (!showAdvancedSync) {
        syncTabSpacerWidget = new QSpacerItem(
                    0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding);
        ui->syncTab->layout()->addItem(syncTabSpacerWidget);
    }
}

void MainWindow::on_advancedSyncLocalPathInput_selectionChanged() {
    if (!ui->advancedSyncLocalPathInput->hasFocus()) {
        return;
    }
    QFileDialog fileSelectDialog(this);
    QDir::Filters syncFilter =
            QDir::AllEntries | QDir::NoSymLinks | QDir::NoDotAndDotDot;
    fileSelectDialog.setFilter(syncFilter);
    fileSelectDialog.setFileMode(QFileDialog::Directory);
    if (fileSelectDialog.exec()) {
        QString filename("");
        QStringList selectedFiles = fileSelectDialog.selectedFiles();
        if (!selectedFiles.isEmpty()) {
            filename = selectedFiles.last();
        }
        ui->advancedSyncLocalPathInput->setText(filename);
    }
}

void MainWindow::on_advancedSyncLocalPathInput_textEdited(const QString &) {
    updateAddAdvancedEnabled();
}

void MainWindow::on_advancedSyncRemotePathInput_textChanged(const QString &) {
    QString remoteText = ui->advancedSyncRemotePathInput->text();
    if (!remoteText.startsWith("/")) {
        remoteText.prepend("/");
        ui->advancedSyncRemotePathInput->setText(remoteText);
    }
    updateAddAdvancedEnabled();
}

void MainWindow::updateAddAdvancedEnabled() {
    QString localText = ui->advancedSyncLocalPathInput->text();
    QFileInfo localFileInfo(localText);
    bool validLocal = localFileInfo.exists() &&
            localFileInfo.isReadable() &&
            localFileInfo.isWritable() &&
            (localFileInfo.isFile() ||
             localFileInfo.isDir());
    QString remoteText = ui->advancedSyncRemotePathInput->text();
    bool validRemote = remoteText.isEmpty() || remoteText.startsWith("/");
    ui->advancedSyncApplyAddPath->setEnabled(validLocal && validRemote);
}

}  // namespace mfsync
}  // namespace com
