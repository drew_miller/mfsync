﻿/*
 * Copyright 2014 Andrew William Miller
 */

#include "com/mfsync/DatabaseManager.h"

#define PREPARE(query, sql) \
    if (!query.prepare(sql)) { \
    if (query.lastError().isValid()) { \
    qDebug() << query.lastError().text(); \
    } \
    Q_ASSERT(!"Prepare query failed"); \
    }

#define EXECUTE(query) \
    if (!query.exec()) { \
    QSqlError lastError = query.lastError(); \
    if (query.lastError().isValid()) { \
    qDebug() << query.lastError().text(); \
    } \
    Q_ASSERT(!"Execute query failed"); \
    }

#define EXECUTE2(query, sql) \
    if (!query.exec(sql)) { \
    QSqlError lastError = query.lastError(); \
    if (query.lastError().isValid()) { \
    qDebug() << query.lastError().text(); \
    } \
    Q_ASSERT(!"Execute query failed"); \
    }

#define COMMIT(db) \
    if (!db.commit()) { \
    qDebug() << db.lastError().text(); \
    Q_ASSERT(false); \
    }

#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QtGlobal>
#include <QtCore/QRegularExpression>
#include <QtCore/QSet>
#include <QtCore/QStandardPaths>
#include <QtCore/QString>
#include <QtCore/QVariant>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlResult>

#include "com/mfsync/types/MappedPath.h"

namespace com {
namespace mfsync {

DatabaseManager::DatabaseManager(
        QString name,
        QObject *parent) :
    QObject(parent),
    name(name),
    db(QSqlDatabase::addDatabase("QSQLITE")) {
}

DatabaseManager::~DatabaseManager() {
    db.close();
}

void DatabaseManager::init() {
    QString databasePath = getDatabasePath(name);
    QDir databaseDir = QFileInfo(databasePath).dir();
    if (!(databaseDir.exists())) {
        QDir::root().mkpath(databaseDir.absolutePath());
    }
    db.setDatabaseName(databasePath);
    qDebug() << "Using database at" << databasePath;
    X_ASSERT(db.open());
    QSqlQuery query(db);
    EXECUTE2(query, "PRAGMA page_size = 4096");
//    EXECUTE2(query, "PRAGMA cache_size = 20000");
    QString createPathMappingTableQueryStr = QString(
                "CREATE TABLE path_mappings ( "
                "mapping INTEGER PRIMARY KEY AUTOINCREMENT, "
                "local_path TEXT UNIQUE NOT NULL COLLATE NOCASE, "
                "remote_path TEXT UNIQUE NOT NULL COLLATE NOCASE, "
                "password TEXT "
                ")").simplified();
    EXECUTE2(query,
             "SELECT sql FROM sqlite_master WHERE tbl_name='path_mappings'");
    if (query.next()) {
        QString previousCreateTableQueryStr = query.value("sql").toString();
        if (createPathMappingTableQueryStr !=
                previousCreateTableQueryStr.simplified()) {
            EXECUTE2(query, "DROP TABLE path_mappings");
            EXECUTE2(query, createPathMappingTableQueryStr);
        }
    } else {
        EXECUTE2(query, createPathMappingTableQueryStr);
    }
    // TODO: keep hashes of remote files
    QString createFilePropertiesTableQueryStr = QString(
                "CREATE TABLE file_properties ( "
                "pathMapping INT, "
                "pathRelative TEXT NOT NULL COLLATE NOCASE, "
                "parentPathRelative TEXT COLLATE NOCASE, "
                "lastSync_exists INT DEFAULT 0, "
                "lastSync_isFolder INT DEFAULT 0, "
                "lastSync_fileHash TEXT DEFAULT NULL, "
                "lastSync_local_lastModified INT, "
                "lastSync_remote_lastModified INT, "
                "lastSync_remote_revision TEXT, "
                "current_local_exists INT DEFAULT 0, "
                "current_local_isFolder INT DEFAULT 0, "
                "current_local_fileHash TEXT DEFAULT NULL, "
                "current_local_lastModified INT, "
                "current_local_status INT DEFAULT 1, "
                "current_remote_exists INT DEFAULT 0, "
                "current_remote_isFolder INT DEFAULT 0, "
                "current_remote_lastModified INT, "
                "current_remote_revision TEXT DEFAULT NULL, "
                "current_remote_status INT DEFAULT 1, "
                "FOREIGN KEY(pathMapping) REFERENCES path_mappings(mapping)"
                " ON DELETE CASCADE, "
                "PRIMARY KEY(pathMapping, pathRelative) "
                ")").simplified();
    EXECUTE2(query,
             "SELECT sql FROM sqlite_master WHERE tbl_name='file_properties'");
    if (query.next()) {
        QString previousCreateTableQueryStr = query.value("sql").toString();
        if (previousCreateTableQueryStr.simplified() ==
                createFilePropertiesTableQueryStr) {
            QString query_str;
            query_str = QString(
                        "UPDATE file_properties SET "
                        "current_local_status=:unverified, "
                        "current_remote_status ="
                        " min(:verified,current_remote_status)");
            PREPARE(query, query_str);
            query.bindValue(":unverified", FileStatus::Unverified);
            query.bindValue(":verified", FileStatus::Verified);
            EXECUTE(query);
            query_str = QString(
                        "UPDATE file_properties SET "
                        "current_remote_status = :unverified "
                        "WHERE current_remote_status < :verified");
            PREPARE(query, query_str);
            query.bindValue(":unverified", FileStatus::Unverified);
            query.bindValue(":verified", FileStatus::Verified);
            EXECUTE(query);
        } else {
            EXECUTE2(query, "DROP TABLE file_properties");
            EXECUTE2(query, createFilePropertiesTableQueryStr);
        }
    } else {
        EXECUTE2(query, createFilePropertiesTableQueryStr);
    }
    EXECUTE2(query, "PRAGMA foreign_keys=ON");
    EXECUTE2(query, "PRAGMA case_sensitive_like=ON");
}

void DatabaseManager::reset() {
    QString databaseName = db.databaseName();
    db.removeDatabase(databaseName);
    QFile::remove(databaseName);
    init();
}

QString DatabaseManager::getBasePath(int mapping, bool isRemote) {
    QString key;
    QSqlQuery query(db);
    QString query_str = QString(
                "SELECT %1_path "
                "FROM path_mappings "
                "WHERE mapping = :mapping ")
            .arg(isRemote ? "remote" : "local");
    PREPARE(query, query_str);
    query.bindValue(":mapping", mapping);
    EXECUTE(query);
    if (query.next()) {
        key = query.value(0).toString();
    }
    return key;
}

int DatabaseManager::addPathMapping(
        QString localPath,
        QString remotePath,
        QString password) {
    QSqlQuery query(db);
    QString query_str = QString(
                "INSERT INTO path_mappings("
                "local_path, "
                "remote_path, "
                "password "
                ") VALUES (?, ?, ?)");
    PREPARE(query, query_str);
    query.addBindValue(localPath);
    query.addBindValue(remotePath);
    query.addBindValue(password);
    EXECUTE(query);
    query_str = QString(
                "SELECT mapping "
                "FROM path_mappings "
                "WHERE local_path = :local_path "
                "AND remote_path = :remote_path");
    PREPARE(query, query_str);
    query.bindValue(":local_path", localPath);
    query.bindValue(":remote_path", remotePath);
    EXECUTE(query);
    X_ASSERT(query.next());
    int mappingKey = query.value("mapping").toInt();
    return mappingKey;
}

int DatabaseManager::removePathMapping(
        int mappingId) {
    QSqlQuery query(db);
    QString query_str = QString(
                "DELETE FROM path_mappings "
                "WHERE mapping = :mapping");
    PREPARE(query, query_str);
    query.bindValue(":mapping", mappingId);
    EXECUTE(query);
    qDebug() << "D" << query.numRowsAffected();
    return query.numRowsAffected();
}

PathMappingEntry DatabaseManager::getPathMapping(int id) {
    QSqlQuery query(db);
    QString query_str;
    query_str = QString(
                "SELECT * FROM path_mappings "
                "WHERE mapping = :id");
    PREPARE(query, query_str);
    query.bindValue(":id", id);
    EXECUTE(query);
    PathMappingEntry pathMappingEntry;
    pathMappingEntry.id = id;
    if (query.next()) {
        pathMappingEntry.localPath = query.value("local_path").toString();
        pathMappingEntry.remotePath = query.value("remote_path").toString();
        pathMappingEntry.password = query.value("password").toString();
    }
    return pathMappingEntry;
}

QList<PathMappingEntry> DatabaseManager::getPathMappings() {
    QList<PathMappingEntry> pathMappings;
    QSqlQuery query(db);
    EXECUTE2(query, "SELECT * FROM path_mappings");
    while (query.next()) {
        PathMappingEntry entry;
        entry.id = query.value("mapping").toInt();
        entry.localPath = query.value("local_path").toString();
        entry.remotePath = query.value("remote_path").toString();
        entry.password = query.value("password").toString();
        pathMappings.append(entry);
    }
    return pathMappings;
}

/// Stuff for MainWindow ///////////////////////////////////////////////////////

double DatabaseManager::getSyncedCount() {
    QSqlQuery query(db);
    QString query_str = QString(
                "SELECT count(*) FROM file_properties WHERE "
                "current_local_status = :synced AND "
                "current_remote_status = :synced");
    PREPARE(query, query_str);
    query.bindValue(":synced", FileStatus::Synced);
    EXECUTE(query);
    double count = 0;
    X_ASSERT(query.next());
    count = query.value(0).toDouble();
    return count;
}

double DatabaseManager::getIndexingCount() {
    QSqlQuery query(db);
    QString query_str = QString(
                "SELECT count(*) FROM file_properties WHERE "
                "current_local_status < :verified OR "
                "current_remote_status < :verified");
    PREPARE(query, query_str);
    query.bindValue(":verified", FileStatus::Verified);
    EXECUTE(query);
    double count = 0;
    X_ASSERT(query.next());
    count = query.value(0).toDouble();
    return count;
}

double DatabaseManager::getSyncingCount() {
    QSqlQuery query(db);
    QString query_str = QString(
                "SELECT count(*) FROM file_properties WHERE "
                "current_local_status >= :verified AND "
                "current_local_status < :synced AND "
                "current_remote_status >= :verified AND "
                "current_remote_status < :synced");
    PREPARE(query, query_str);
    query.bindValue(":verified", FileStatus::Verified);
    query.bindValue(":synced", FileStatus::Synced);
    EXECUTE(query);
    double count = 0;
    X_ASSERT(query.next());
    count = query.value(0).toDouble();
    return count;
}

/// File status ////////////////////////////////////////////////////////////////

FileStatus DatabaseManager::getStatus(
        MappedPath path,
        bool isRemote) {
    QSqlQuery query(db);
    QString query_str = QString(
                "SELECT current_%1_status "
                "FROM file_properties "
                "WHERE pathMapping = :pathMapping "
                "AND pathRelative = :pathRelative")
            .arg(isRemote ? "remote" : "local");
    PREPARE(query, query_str);
    query.bindValue(":pathMapping", path.getMapping());
    query.bindValue(":pathRelative", path.getRelativePath());
    EXECUTE(query);
    FileStatus status = FileStatus::Unknown;
    if (query.next()) {
        status = (FileStatus) query.value(0).toInt();
    }
    return status;
}

void DatabaseManager::setStatus(
        MappedPath path,
        FileStatus status,
        bool setLocal,
        bool setRemote) {
    if (!setLocal && !setRemote) {
        return;
    }
    if (status == FileStatus::Synced || status == FileStatus::Syncing) {
        setLocal = true;
        setRemote = true;
    }
    FilePropertiesEntry fileProperties = getFileProperties(path);
    if (setLocal) {
        if (status == FileStatus::Unverified) {
            fileProperties.current_local.exists = true;
        }
        setFileProperties(fileProperties.current_local, status);
    }
    if (setRemote) {
        if (status == FileStatus::Unverified) {
            fileProperties.current_remote.exists = true;
        }
        setFileProperties(fileProperties.current_remote, status);
    }
}

void DatabaseManager::setChildrenStatus(
        MappedPath path,
        FileStatus status,
        bool setLocal,
        bool setRemote,
        bool recursive) {
    if (!setLocal && !setRemote) {
        return;
    }
    if (status == Synced || status == Syncing) {
        setLocal = true;
        setRemote = true;
    }
    QSqlQuery query(db);
    QString query_str = "UPDATE file_properties SET ";
    if (setLocal) {
        query_str += "current_local_status = :status";
        if (setRemote) {
            query_str += ",";
        }
        query_str += " ";
    }
    if (setRemote) {
        query_str += "current_remote_status = :status ";
    }
    query_str += "WHERE pathMapping = :pathMapping ";
    if (recursive) {
        query_str += QString(
                    "AND parentPathRelative LIKE '%1' ESCAPE '\\'")
                .arg(pathStartPattern(path.getRelativePath()));
        PREPARE(query, query_str);
    } else {
        query_str += "AND parentPathRelative = :parentPath";
        PREPARE(query, query_str);
        query.bindValue(":parentPath", path.getRelativePath());
    }
    query.bindValue(":pathMapping", path.getMapping());
    query.bindValue(":status", status);
    EXECUTE(query);
}

/// Paths //////////////////////////////////////////////////////////////////////

bool DatabaseManager::fileExists(
        MappedPath path,
        bool isRemote) {
    QSqlQuery query(db);
    QString query_str = QString(
                "SELECT * FROM file_properties"
                " WHERE pathMapping=:pathMapping"
                " AND pathRelative=:pathRelative"
                " AND current_%1_exists=1;")
            .arg(isRemote ? "remote" : "local");
    PREPARE(query, query_str);
    query.bindValue(":pathMapping", path.getMapping());
    query.bindValue(":pathRelative", path.getRelativePath());
    query.exec();
    bool exists = query.next();
    return exists;
}

QList<MappedPath> DatabaseManager::getUnverified(
        bool isRemote,
        int limit) {
    QList<MappedPath> unverified;
    QSqlQuery query(db);
    QString query_str = QString(
                "SELECT pathMapping, pathRelative "
                "FROM file_properties "
                "WHERE "
                "current_%1_status = :unverified AND "
                "pathMapping+parentPathRelative IN "
                "( SELECT pathMapping+pathRelative "
                "  FROM file_properties WHERE "
                "  ( pathRelative IS NULL OR "
                "    pathRelative = '' "
                "  ) OR "
                "  current_%1_status = :verified "
                ") "
                "LIMIT :limit")
            .arg(isRemote ? "remote" : "local");
    PREPARE(query, query_str);
    bindQueryFileStatuses(query);
    query.bindValue(":limit", limit);
    EXECUTE(query);
    while (query.next()) {
        unverified.append(queryToMappedPath(query));
    }
    return unverified;
}

QList<MappedPath> DatabaseManager::getVerifiedUnsynced(
        int limit) {
    QList<MappedPath> verifiedUnsynced;
    QSqlQuery query(db);
    QString query_str = QString(
                "SELECT pathMapping, pathRelative "
                "FROM file_properties "
                "WHERE "
                "current_local_status = :verified AND "
                "pathMapping+parentPathRelative IN "
                "( SELECT pathMapping+pathRelative "
                "  FROM file_properties "
                "  WHERE "
                "  ( pathRelative IS NULL OR "
                "    pathRelative = '' "
                "  ) OR "
                "  current_local_status = :synced "
                ") "
                "LIMIT :limit");
    PREPARE(query, query_str);
    bindQueryFileStatuses(query);
    query.bindValue(":limit", limit);
    EXECUTE(query);
    while (query.next()) {
        verifiedUnsynced.append(queryToMappedPath(query));
    }
    return verifiedUnsynced;
}

QSet<MappedPath> DatabaseManager::getFilesWithStatus(
        FileStatus status,
        bool getLocal,
        bool getRemote,
        int limit) {
    QSet<MappedPath> matchingFiles;
    if (!getLocal && !getRemote) {
        return matchingFiles;
    }
    QSqlQuery query(db);
    QString query_str = QString(
                "SELECT * FROM file_properties WHERE ");
    if (getLocal) {
        query_str.append("current_local_status=:status ");
        if (getRemote) {
            query_str.append("OR ");
        }
    }
    if (getRemote) {
        query_str.append("current_remote_status=:status ");
    }
    query_str.append("ORDER BY LENGTH(pathRelative) ASC ");
    query_str.append("LIMIT :limit");
    PREPARE(query, query_str);
    query.bindValue(":status", status);
    query.bindValue(":limit", limit);
    EXECUTE(query);
    while (query.next()) {
        FilePropertiesEntry properties = queryToFileProperties(query);
        matchingFiles.insert(properties.current_local.path);
    }
    return matchingFiles;
}

QSet<MappedPath> DatabaseManager::folderContents(
        MappedPath path,
        bool isRemote) {
    QSet<MappedPath> folderContents;
    QSqlQuery query(db);
    QString query_str = QString(
                "SELECT * FROM file_properties "
                "WHERE pathMapping=:pathMapping "
                "AND parentPathRelative LIKE '%1' ESCAPE '\\'"
                "AND current_%2_exists=1")
            .arg(pathStartPattern(path.getRelativePath()))
            .arg(isRemote ? "remote" : "local");
    PREPARE(query, query_str);
    query.bindValue(":pathMapping", path.getMapping());
    EXECUTE(query);
    while (query.next()) {
        FilePropertiesEntry properties = queryToFileProperties(query);
        folderContents.insert(properties.current_local.path);
    }
    return folderContents;
}

FilePropertiesEntry DatabaseManager::getFileProperties(
        MappedPath mappedPath) {
    QSqlQuery query(db);
    QString query_str = QString(
                "SELECT * FROM file_properties "
                "WHERE pathMapping=:pathMapping "
                "AND pathRelative=:pathRelative");
    PREPARE(query, query_str);
    query.bindValue(":pathMapping", mappedPath.getMapping());
    query.bindValue(":pathRelative", mappedPath.getRelativePath());
    EXECUTE(query);
    FilePropertiesEntry properties;
    if (query.next()) {
        properties = queryToFileProperties(query);
    } else {
        properties.lastSync_local.exists = false;
        properties.lastSync_remote.exists = false;
        properties.current_local.exists = false;
        properties.current_remote.exists = false;
        properties.lastSync_local.path = mappedPath;
        properties.lastSync_remote.path = mappedPath;
        properties.current_local.path = mappedPath;
        properties.current_remote.path = mappedPath;
    }
    return properties;
}

void DatabaseManager::setFileProperties(
        LocalFileInfo localFile,
        FileStatus status) {
    qDebug() << "local" << localFile.path << (int)status;
    X_ASSERT(db.transaction());
    localFile.isDir &= localFile.exists;
    if (!localFile.exists || localFile.isDir) {
        localFile.fileHash.clear();
    }
    QSqlQuery query(db);
    QString query_str;
    if (!localFile.exists) {
        query_str = QString(
                    "UPDATE file_properties "
                    "SET current_local_exists=0, "
                    "current_local_lastModified=:lastModified, "
                    "current_local_status=:status "
                    "WHERE pathMapping=:pathMapping AND "
                    "pathRelative=:pathRelative");
        PREPARE(query, query_str);
        query.bindValue(":status", status);
        query.bindValue(":lastModified", localFile.lastModified);
        query.bindValue(":pathMapping", localFile.path.getMapping());
        query.bindValue(":pathRelative", localFile.path.getRelativePath());
        EXECUTE(query);
        cleanDeletedFiles();
    } else {
        query_str = QString(
                    "UPDATE file_properties "
                    "SET parentPathRelative=:parentPathRelative, "
                    "current_local_exists=:exists, "
                    "current_local_isFolder=:isFolder, "
                    "current_local_fileHash=:fileHash, "
                    "current_local_lastModified=:lastModified, "
                    "current_local_status=:status "
                    "WHERE ( "
                    "pathMapping=:pathMapping "
                    "AND pathRelative=:pathRelative "
                    ")");
        PREPARE(query, query_str);
        query.bindValue(":pathMapping", localFile.path.getMapping());
        query.bindValue(":pathRelative", localFile.path.getRelativePath());
        query.bindValue(":parentPathRelative",
                        parentPath(localFile.path.getRelativePath()));
        query.bindValue(":exists", localFile.exists);
        query.bindValue(":isFolder", localFile.isDir);
        query.bindValue(":fileHash", localFile.fileHash);
        query.bindValue(":lastModified", localFile.lastModified);
        query.bindValue(":status", status);
        EXECUTE(query);
        if (query.numRowsAffected() == 0) {
            query_str = QString(
                        "INSERT OR IGNORE INTO file_properties("
                        "pathMapping, "
                        "pathRelative, "
                        "parentPathRelative, "
                        "current_local_exists, "
                        "current_local_isFolder, "
                        "current_local_fileHash, "
                        "current_local_lastModified, "
                        "current_local_status"
                        ") VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            PREPARE(query, query_str);
            query.addBindValue(localFile.path.getMapping());
            query.addBindValue(localFile.path.getRelativePath());
            query.addBindValue(parentPath(localFile.path.getRelativePath()));
            query.addBindValue(localFile.exists);
            query.addBindValue(localFile.isDir);
            query.addBindValue(localFile.fileHash);
            query.addBindValue(localFile.lastModified);
            query.addBindValue(status);
            EXECUTE(query);
        }
    }
    if (status == Synced || status == Syncing) {
        query_str = QString(
                    "UPDATE file_properties SET "
                    "current_local_status = :status, "
                    "current_remote_status = :status");
        if (status == Synced) {
            query_str += QString(
                        ", "
                        "lastSync_exists = current_local_exists, "
                        "lastSync_isFolder = current_local_isFolder, "
                        "lastSync_fileHash = current_local_fileHash, "
                        "lastSync_local_lastModified = current_local_lastModified, "
                        "lastSync_remote_lastModified = current_remote_lastModified, "
                        "lastSync_remote_revision = current_remote_revision");
        }
        query_str += QString(
                    " WHERE "
                    "pathMapping = :pathMapping AND "
                    "pathRelative = :pathRelative");
        X_ASSERT(query.prepare(query_str));
        query.bindValue(":status", status);
        query.bindValue(":pathMapping", localFile.path.getMapping());
        query.bindValue(":pathRelative", localFile.path.getRelativePath());
        X_ASSERT(query.exec());
    }
    updateSyncStatus();
    COMMIT(db)
}

void DatabaseManager::setFileProperties(
        RemoteFileInfo remoteFile,
        FileStatus status) {
    qDebug() << "remote" << remoteFile.path << (int)status;
    X_ASSERT(db.transaction());
    remoteFile.isDir &= remoteFile.exists;
    QSqlQuery query(db);
    QString query_str;
    if (!remoteFile.exists) {
        query_str = QString(
                    "UPDATE file_properties "
                    "SET "
                    "current_remote_exists = 0, "
                    "current_remote_lastModified = :lastModified, "
                    "current_remote_revision = :revision, "
                    "current_remote_status = :status "
                    "WHERE "
                    "pathMapping = :pathMapping AND "
                    "pathRelative = :pathRelative ");
        PREPARE(query, query_str);
        query.bindValue(":lastModified", remoteFile.lastModified);
        query.bindValue(":revision", remoteFile.revision);
        query.bindValue(":status", status);
        query.bindValue(":pathMapping", remoteFile.path.getMapping());
        query.bindValue(":pathRelative", remoteFile.path.getRelativePath());
        EXECUTE(query);
        cleanDeletedFiles();
    } else {
        query_str = QString(
                    "UPDATE file_properties "
                    "SET "
                    "parentPathRelative = :parentPathRelative, "
                    "current_remote_exists = :exists, "
                    "current_remote_isFolder = :isFolder, "
                    "current_remote_lastModified = :lastModified, "
                    "current_remote_revision = :revision, "
                    "current_remote_status = :status "
                    "WHERE "
                    "pathMapping = :pathMapping "
                    "AND pathRelative = :pathRelative");
        PREPARE(query, query_str);
        query.bindValue(":pathMapping", remoteFile.path.getMapping());
        query.bindValue(":pathRelative", remoteFile.path.getRelativePath());
        query.bindValue(":parentPathRelative",
                        parentPath(remoteFile.path.getRelativePath()));
        query.bindValue(":exists", remoteFile.exists);
        query.bindValue(":isFolder", remoteFile.isDir);
        query.bindValue(":lastModified", remoteFile.lastModified);
        query.bindValue(":revision", remoteFile.revision);
        query.bindValue(":status", status);
        EXECUTE(query);
        if (query.numRowsAffected() == 0) {
            query_str = QString(
                        "INSERT OR IGNORE INTO file_properties("
                        "pathMapping, "
                        "pathRelative, "
                        "parentPathRelative, "
                        "current_remote_exists, "
                        "current_remote_isFolder, "
                        "current_remote_lastModified, "
                        "current_remote_revision, "
                        "current_remote_status"
                        ") VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            PREPARE(query, query_str);
            query.addBindValue(remoteFile.path.getMapping());
            query.addBindValue(remoteFile.path.getRelativePath());
            query.addBindValue(parentPath(remoteFile.path.getRelativePath()));
            query.addBindValue(remoteFile.exists);
            query.addBindValue(remoteFile.isDir);
            query.addBindValue(remoteFile.lastModified);
            query.addBindValue(remoteFile.revision);
            query.addBindValue(status);
            EXECUTE(query);
        }
    }
    if (status == Synced || status == Syncing) {
        query_str = QString(
                    "UPDATE file_properties SET "
                    "current_local_status = :status, "
                    "current_remote_status = :status");
        if (status == Synced) {
            query_str += QString(
                        ", "
                        "lastSync_exists = current_remote_exists, "
                        "lastSync_isFolder = current_remote_isFolder, "
                        "lastSync_fileHash = current_local_fileHash, "
                        "lastSync_local_lastModified = current_local_lastModified, "
                        "lastSync_remote_lastModified = current_remote_lastModified, "
                        "lastSync_remote_revision = current_remote_revision");
        }
        query_str += QString(
                    " WHERE "
                    "pathMapping = :pathMapping AND "
                    "pathRelative = :pathRelative");
        X_ASSERT(query.prepare(query_str));
        query.bindValue(":status", status);
        query.bindValue(":pathMapping", remoteFile.path.getMapping());
        query.bindValue(":pathRelative", remoteFile.path.getRelativePath());
        X_ASSERT(query.exec());
    }
    updateSyncStatus();
    COMMIT(db);
}

void DatabaseManager::updateSyncStatus() {
    QSqlQuery query(db);
    QString query_str;
    // If files haven't changed since they were last synced, then they're still
    // synced.
    query_str = QString(
                "UPDATE file_properties SET "
                "current_local_status = max(:verified, current_remote_status) "
                "WHERE "
                "current_local_status > :verified AND "
                "current_local_status > current_remote_status");
    PREPARE(query, query_str);
    query.bindValue(":verified", FileStatus::Verified);
    query.bindValue(":synced", FileStatus::Synced);
    EXECUTE(query);
    query_str = QString(
                "UPDATE file_properties SET "
                "current_remote_status = max(:verified, current_local_status) "
                "WHERE "
                "current_remote_status > :verified AND "
                "current_remote_status > current_local_status");
    PREPARE(query, query_str);
    query.bindValue(":verified", FileStatus::Verified);
    query.bindValue(":synced", FileStatus::Synced);
    EXECUTE(query);
    query_str = QString(
                "UPDATE file_properties SET "
                "current_local_status = :synced, "
                "current_remote_status = :synced, "
                "lastSync_local_lastModified = current_local_lastModified, "
                "lastSync_remote_lastModified = current_remote_lastModified "
                "WHERE "
                "current_local_status >= :verified AND "
                "current_remote_status >= :verified AND "
                "lastSync_exists = current_local_exists AND "
                "lastSync_isFolder = current_local_isFolder AND "
                "lastSync_fileHash = current_local_fileHash AND "
                "lastSync_exists = current_remote_exists AND "
                "lastSync_isFolder = current_remote_isFolder AND "
                "lastSync_remote_revision = current_remote_revision");
    PREPARE(query, query_str);
    query.bindValue(":verified", FileStatus::Verified);
    query.bindValue(":synced", FileStatus::Synced);
    EXECUTE(query);
}

void DatabaseManager::invalidateAllRemote() {
    QSqlQuery query(db);
    QString query_str = QString(
                "UPDATE file_properties SET current_remote_status=1");
    PREPARE(query, query_str);
    EXECUTE(query);
}

void DatabaseManager::bindQueryFileStatuses(QSqlQuery &query) {
    query.bindValue(":unverified", FileStatus::Unverified);
    query.bindValue(":verifying", FileStatus::Verifying);
    query.bindValue(":verified", FileStatus::Verified);
    query.bindValue(":syncing", FileStatus::Syncing);
    query.bindValue(":synced", FileStatus::Synced);
}

QString DatabaseManager::getDatabasePath(QString name) {
    return QStandardPaths::writableLocation(QStandardPaths::DataLocation) +
            QDir::separator() + QString(name+".sqlite");
}

void DatabaseManager::cleanDeletedFiles() {
    QSqlQuery query(db);
    QString query_str = QString(
                "DELETE FROM file_properties WHERE "
                "current_local_status >= :verified "
                "AND current_local_exists = 0 "
                "AND current_remote_status >= :verified "
                "AND current_remote_exists=0 ");
    PREPARE(query, query_str);
    query.bindValue(":verified", FileStatus::Verified);
    EXECUTE(query);
}

FilePropertiesEntry DatabaseManager::queryToFileProperties(
        const QSqlQuery &query) {
    FilePropertiesEntry fileProperties;
    // File path
    fileProperties.lastSync_local.path =
            fileProperties.current_local.path =
            fileProperties.current_remote.path =
            fileProperties.lastSync_remote.path = MappedPath(
                query.value("pathMapping").toInt(),
                query.value("pathRelative").toString());
    // LastSync
    fileProperties.lastSync_local.exists =
            fileProperties.lastSync_remote.exists =
            query.value("lastSync_exists").toBool();
    fileProperties.lastSync_local.isDir =
            fileProperties.lastSync_remote.isDir =
            query.value("lastSync_isFolder").toBool();
    fileProperties.lastSync_local.fileHash =
            query.value("lastSync_fileHash").toString();
    // LastSync Local
    fileProperties.lastSync_local.lastModified =
            query.value("lastSync_local_lastModified").toLongLong();
    // LastSync Remote
    fileProperties.lastSync_remote.lastModified =
            query.value("lastSync_remote_lastModified").toULongLong();
    fileProperties.lastSync_remote.revision =
            query.value("lastSync_remote_revision").toString();
    // Current Local
    fileProperties.current_local.exists =
            query.value("current_local_exists").toBool();
    fileProperties.current_local.isDir =
            query.value("current_local_isFolder").toBool();
    fileProperties.current_local.fileHash =
            query.value("current_local_fileHash").toString();
    fileProperties.current_local.lastModified =
            query.value("current_local_lastModified").toLongLong();
    fileProperties.current_local_status =
            (FileStatus) query.value("current_local_status").toInt();
    // Current Remote
    fileProperties.current_remote.exists =
            query.value("current_remote_exists").toBool();
    fileProperties.current_remote.isDir =
            query.value("current_remote_isFolder").toBool();
    fileProperties.current_remote.lastModified =
            query.value("current_remote_lastModified").toULongLong();
    fileProperties.current_remote.revision =
            query.value("current_remote_revision").toString();
    fileProperties.current_remote_status =
            (FileStatus) query.value("current_remote_status").toInt();
    return fileProperties;
}

MappedPath DatabaseManager::queryToMappedPath(
        const QSqlQuery &query) {
    MappedPath mappedPath;
    mappedPath.setMapping(query.value("pathMapping").toInt());
    mappedPath.setRelativePath(query.value("pathRelative").toString());
    return mappedPath;
}

QString DatabaseManager::parentPath(QString relativePath) {
    if ((!relativePath.contains("/")) || (relativePath == "/")) {
        return "";
    }
    Q_ASSERT(!relativePath.endsWith("/"));
    return relativePath.left(relativePath.lastIndexOf("/"));
}

QString DatabaseManager::pathStartPattern(QString path) {
    return path.replace("/+", "/")
            .remove(QRegularExpression("/*$"))
            .replace("\\", "\\\\")
            .replace("_", "\\_")
            .replace("%", "\\%")
            .replace("'", "''")
            .append("/%");
}

}  // namespace mfsync
}  // namespace com
