/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef COM_MFSYNC_ENCRYPTION_H_
#define COM_MFSYNC_ENCRYPTION_H_

#include <util/common.h>

#include <openssl/evp.h>
#include <stddef.h>
#include <QtCore/QIODevice>
#include <QtCore/QObject>
#include <QtCore/QString>

#define AES_ROUNDS 8
#define AES_KEYLEN 1024

namespace com {
namespace mfsync {

class Encryption : public QObject {
    Q_OBJECT

 public:
    explicit Encryption();
    ~Encryption();

 public slots:
    static bool encrypt(
            QIODevice *input,
            QIODevice *output,
            QString password,
            bool *cancel);
    static bool decrypt(
            QIODevice *input,
            QIODevice *output,
            QString password,
            bool *cancel);
    static bool hash(
            QIODevice *input,
            QByteArray *result,
            bool *cancel);

 private:
    static bool copyFile(
            QIODevice *input,
            QIODevice *output,
            bool *cancel);
    static void initializeContext(
            EVP_CIPHER_CTX *aesEncryptCtx,
            unsigned char *aesPass,
            unsigned char **aesKey,
            unsigned char **aesIV);
    static unsigned char *passwordToAesPass(
            QString password);
};

}  // namespace mfsync
}  // namespace com

#endif  // COM_MFSYNC_ENCRYPTION_H_
