/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef COM_MFSYNC_SYNCER_H_
#define COM_MFSYNC_SYNCER_H_

#include <util/common.h>

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QFileSystemWatcher>
#include <QtCore/QFuture>
#include <QtCore/QFutureWatcher>
#include <QtCore/QJsonObject>
#include <QtCore/QMap>
#include <QtCore/QMutex>
#include <QtCore/QObject>
#include <QtCore/QSet>
#include <QtCore/QString>
#include <QtCore/QVariantList>
#include <QtCore/QVariantMap>

#include "com/mfsync/types/MappedPath.h"
#include "com/mfsync/types/LocalFileInfo.h"
#include "com/mfsync/types/RemoteFileInfo.h"
#include "com/mfsync/DatabaseManager.h"
#include "com/mfsync/Encryption.h"

#define MAX_SYNCING 2
#define MAX_LOCAL_VERIFYING 2
#define MAX_REMOTE_VERIFYING 4

namespace com {
namespace mfsync {

class Control;
class FileTransfer;

class Syncer : public QObject {
    Q_OBJECT

 public:
    explicit Syncer(
            Control *control);
    ~Syncer();

 public slots:
    void init();
    void verifyFs();
    void verifyDb();

 private slots:
    void verifyLocal(QString localPath);
    void verifyRemote(QString remotePath);

 private:
    struct HashContext {
        QFutureWatcher<bool>* futureWatcher;
        QString localPath;
        QFile *localFile;
        QByteArray fileHash;
        bool *cancel;
    };
    QMap<void*, HashContext*> hashFutureWatcherToContext;

 private slots:
    void handle_fileHashReady();

 private:
    void broadcastSyncStatus();

 signals:
    void signal_syncStatusReady(
            double indexingCount,
            double syncingCount,
            double syncedCount);

 public:
    enum SyncAction {
        NoopSynced,
        Verify,
        UploadFile,
        UploadFolder,
        DeleteRemote,
        DownloadFile,
        DownloadFolder,
        DeleteLocal
    };

 public slots:
    void syncFile(MappedPath mappedPath);
    SyncAction getSyncAction(MappedPath mappedPath);

 private slots:
    void handle_remoteMetadataReady(
            QString remotePath,
            QVariantMap metadata);
    void handle_folderCreated(
            QString remotePath,
            QVariantMap metadata);
    void handle_remoteFileRemoved(
            QString remotePath,
            QVariantMap metadata);
    void handle_fileUploadComplete(
            QString localPath,
            QString remotePath,
            QVariantMap metadata);
    void handle_fileDownloadComplete(
            QString localPath,
            QString remotePath,
            QString encryptionPassword,
            QVariantMap metadata);

 public slots:
    void handle_localFileModifiedExternal(
            QString localPath);
    void handle_remoteDeltaReady(
            QVariantList metadataList,
            bool hasMore);
    void handle_remoteDeltaReset();
    // Default sync paths
    QList<PathMappingEntry> getDefaultPathMappings();
    // Path mappings
    int addSyncPathMapping(
            QString localPath,
            QString remotePath,
            QString password);
    int removeSyncPathMappingById(
            int mappingId);
    int removeSyncPathMapping(
            QString localPath,
            QString remotePath);
    QString mappedPathToAbsolute(
            MappedPath path,
            bool isRemote);
    bool isPathSynced(
            QString absolutePath,
            bool isRemote);
    MappedPath absoluteToMappedPath(
            QString absolutePath,
            bool isRemote);

 signals:
    void signal_syncPathsChanged(QList<PathMappingEntry> pathMappings);

 public:
    MappedPath nonconflictingFilepath(
            MappedPath mappedPath,
            bool isRemoteRename);

    Control *control;
    QFileSystemWatcher *fsWatcher;
    DatabaseManager *dbm;
    FileTransfer *fileTransfer;
    bool *isEnding;
    int syncPathsModifiedExternal;
    bool hasFirstDelta = false;
    QDir::Filters syncFilter;

 private:
    bool isModifiedLocal(LocalFileInfo lastSync, LocalFileInfo current);
    bool isModifiedRemote(RemoteFileInfo lastSync, RemoteFileInfo current);
    bool pathHasParent(MappedPath mappedPath);
    MappedPath getParentPath(MappedPath mappedPath);
    bool directoryIsOrContainsFile(QString directory, QString file);
};

}  // namespace mfsync
}  // namespace com

#endif  // COM_MFSYNC_SYNCER_H_
