/*
 * Copyright 2014 Andrew William Miller
 */

#ifndef COM_MFSYNC_DROPBOXAPI_H_
#define COM_MFSYNC_DROPBOXAPI_H_

#include <util/common.h>

#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QFuture>
#include <QtCore/QJsonObject>
#include <QtCore/QMutex>
#include <QtCore/QObject>
#include <QtCore/QSet>
#include <QtCore/QSignalMapper>
#include <QtCore/QSettings>
#include <QtCore/QString>
#include <QtCore/QUrl>
#include <QtNetwork/QNetworkAccessManager>

#include "types/RemoteFileInfo.h"
#include "types/LocalFileInfo.h"
#include "types/UserInformation.h"

#define SETTINGS_KEY_BEARER_TOKEN "bearer_token"
#define SETTINGS_KEY_DELTA_CURSOR "delta_cursor"
#define UPLOAD_CHUNK_SIZE 100000

namespace com {
namespace mfsync {

class Control;

class DropboxAPI : public QObject{
    Q_OBJECT

 public:
    explicit DropboxAPI(
            QSettings *settings);
    ~DropboxAPI();

 public:
    QString getBearerToken();
    void setBearerToken(
            QString bearerToken);

    bool isAuthenticated();

 public:
    void deAuthenticate();

 private slots:
    void handle_deAuthenticateFinished();

 signals:
    void signal_loginStatusChanged();

    ////////////////////////////////////////////////////////////////////////////
    /// User methods ///////////////////////////////////////////////////////////

 public:
    void getAccountInfo();

 private slots:
    void handle_accountInfoReplyFinished();

 signals:
    void signal_accountInfoReady(QVariantMap accountInfo);

    ////////////////////////////////////////////////////////////////////////////
    /// Filesystem watcher

 public:
    void watch();
    void stop();

 private:
    void delta();
    void longPollDelta();
    QMutex deltaMutex;
    QString deltaCursor;

 private slots:
    void handle_deltaReady();
    void handle_longpollDeltaReady();

 signals:
    void signal_deltaReset();
    void signal_deltaReady(QVariantList metadata_list, bool hasMore);

    ////////////////////////////////////////////////////////////////////////////
    /// File metadata

 public slots:
    void getMetadata(QString remotePath);

 private:
    QMap<QNetworkReply*, QString> metadataReplyToRemotePath;

 private slots:
    void handle_metadataReady();

 signals:
    void signal_metadataReady(
            QString remotePath,
            QVariantMap remoteInfo);

    ////////////////////////////////////////////////////////////////////////////
    /// Transfer operations

    // Create folder
 public slots:
    void createFolder(
            QString remotePath);

 private:
    QMap<QNetworkReply*, QString> replyToCreateFolderPath;

 private slots:
    void handle_folderCreateFinished();

 signals:
    void signal_folderCreated(
            QString remotePath,
            QVariantMap metadata);

    // Remove file/folder
 public slots:
    void deleteFile(
            QString remotePath);

 private:
    QMap<QNetworkReply*, QString> replyToRemoveFilePath;

 private slots:
    void handle_deleteFileFinished();

 signals:
    void signal_fileRemoved(
            QString remotePath,
            QVariantMap metadata);

    // Upload file
 public slots:
    void uploadFile(
            QFile *localFile,
            QString remotePath);

 private:
    void uploadFile(
            QFile *localFile,
            QString remotePath,
            QString uploadId);
    QMap<QNetworkReply*, QFile*> replyToUploadingLocalFile;
    QMap<QNetworkReply*, QString> replyToUploadingRemotePath;

 private slots:
    void handle_chunkUploaded();
    void handle_fileUploadCommitFinished();

 signals:
    void signal_fileUploadComplete(
            QFile *localFile,
            QString remotePath,
            QMap<QString, QVariant> metadata);

    // Download file
 public slots:
    void downloadFile(
            QString remotePath,
            QFile *localFile);

 private:
    QMap<QNetworkReply*, QFile*> replyToDownloadLocalFile;
    QMap<QNetworkReply*, QString> replyToDownloadRemotePath;

 private slots:
    void handle_fileDownloadReadReady();
    void handle_fileDownloadFinished();

 signals:
    void signal_fileDownloadComplete(
            QString remotePath,
            QFile *localFile,
            QMap<QString, QVariant> metadata);

    ////////////////////////////////////////////////////////////////////////////
    /// Private stuff //////////////////////////////////////////////////////////

 private:
    void createPathToFile(
            QString file);
    QString escapePath(
            QString remotePath);
    bool addBearerToken(
            QNetworkRequest &request);
    void addParametersToQuery(
            QUrlQuery query,
            QMap<QString, QString> parameters);

    QNetworkAccessManager *manager;
    QString bearerToken;
    QSettings *settings;
    bool isAuthenticated_;
    bool isEnding;
};

}  // namespace mfsync
}  // namespace com

#endif  // COM_MFSYNC_DROPBOXAPI_H_
