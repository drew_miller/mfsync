/*
 * Copyright 2014 Andrew William Miller
 */

#include "com/mfsync/Stuff.h"

#include <util/common.h>

#include <QtCore/QFile>
#include <QtCore/QMetaType>
#include <QtCore/QIODevice>
#include <QtCore/QStandardPaths>
#include <QtNetwork/QSslCertificate>
#include <QtNetwork/QSslConfiguration>
#include <QtNetwork/QSslSocket>
#include <QtCore/QCoreApplication>

#include "com/mfsync/DatabaseManager.h"

namespace com {
namespace mfsync {

void loadSslCertificates() {
    if (!QSslSocket::supportsSsl()) {
        qDebug() << "Error: QSslSocket::supportsSsl() returned false.";
        QCoreApplication::quit();
    }
    // Read the SSL certificate
    {
        QFile file(":/com/mfsync/resources/gd_bundle-g2-g1.crt");
        X_ASSERT(file.open(QIODevice::ReadOnly));
        const QByteArray bytes = file.readAll();
        // Create a certificate object
        const QSslCertificate certificate(bytes);
        // Add this certificate to all SSL connections
        QSslSocket::addDefaultCaCertificate(certificate);
    }
    {
        QFile file(":/com/mfsync/resources/mfsync.com.crt");
        X_ASSERT(file.open(QIODevice::ReadOnly));
        const QByteArray bytes = file.readAll();
        // Create a certificate object
        const QSslCertificate certificate(bytes);
        // Add this certificate to all SSL connections
        QSslSocket::addDefaultCaCertificate(certificate);
    }
    {
        QFile file(":/com/mfsync/resources/dropbox.com.crt");
        X_ASSERT(file.open(QIODevice::ReadOnly));
        const QByteArray bytes = file.readAll();
        // Create a certificate object
        const QSslCertificate certificate(bytes);
        // Add this certificate to all SSL connections
        QSslSocket::addDefaultCaCertificate(certificate);
    }
    // Set the protocol
    QSslConfiguration sslCfg = QSslConfiguration::defaultConfiguration();
    //    sslCfg.setProtocol(QSsl::TlsV1_2);
    QSslConfiguration::setDefaultConfiguration(sslCfg);
}

void registerMetatypes() {
    qRegisterMetaType<QStandardPaths::StandardLocation>("StandardLocation");
    qRegisterMetaType<QMap<QString, QString>>("QMap<QString, QString>");
    qRegisterMetaType<QList<PathMappingEntry>>("QList<PathMappingEntry>");
    qRegisterMetaType<QFile*>("QFile*");
}

}  // namespace mfsync
}  // namespace com
