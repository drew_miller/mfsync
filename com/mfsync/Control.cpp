/*
 * Copyright 2014 Andrew William Miller
 */

#include "com/mfsync/Control.h"

#include <util/StringUtil.h>

#include <QtCore/QCryptographicHash>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QVariant>
#include <QtGui/QGuiApplication>
#include <QtWidgets/QMenu>
#include <QtWidgets/QSystemTrayIcon>

#include "com/mfsync/DropboxAPI.h"
#include "com/mfsync/Syncer.h"
#include "com/mfsync/ui/MainWindow.h"

namespace com {
namespace mfsync {

Control::Control(const QApplication &app) :
    app(&app),
    settings(new QSettings()),
    api(new DropboxAPI(settings)),
    isEnding(false) {
    syncer = new Syncer(this);
    mainWindow = new MainWindow(this);
}

Control::~Control() {
    shutdown();
}

void Control::shutdown() {
    isEnding = true;
    api->stop();
    syncerThread.quit();
    if (!syncerThread.wait(2000)) {
        qWarning("Deadlock in syncerThread, terminating.");
        syncerThread.terminate();
        syncerThread.wait();
    }
}

void Control::start() {
    syncer->dbm->init();
    syncer->moveToThread(&syncerThread);
    syncerThread.start();
    api->watch();
    mainWindow->init();
    mainWindow->show();
    mainWindow->setAttribute(Qt::WA_DeleteOnClose);
    X_ASSERT(connect(mainWindow, SIGNAL(destroyed()),
            this, SLOT(shutdown())));
    X_ASSERT(connect(app, SIGNAL(commitDataRequest(QSessionManager&)),
            this, SLOT(shutdown())));
    QMetaObject::invokeMethod(syncer, "init", Qt::QueuedConnection);
}

}  // namespace mfsync
}  // namespace com
