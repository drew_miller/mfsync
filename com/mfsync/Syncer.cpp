/*
 * Copyright 2014 Andrew William Miller
 */

#include "com/mfsync/Syncer.h"

#include <QtConcurrent/QtConcurrentRun>
#include <QtCore/QtGlobal>
#include <QtCore/QCoreApplication>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QtCore/QJsonArray>
#include <QtCore/QMetaObject>
#include <QtCore/QRegExp>
#include <QtCore/QStandardPaths>
#include <QtCore/QVariant>

#include "com/mfsync/Control.h"
#include "com/mfsync/DropboxAPI.h"
#include "com/mfsync/Encryption.h"
#include "com/mfsync/FileTransfer.h"
#include "com/mfsync/types/LocalFileInfo.h"

#define SETTINGS_KEY_SYNCED_PATHS "synced_paths"

namespace com {
namespace mfsync {

Syncer::Syncer(Control *control)
    : control(control),
      dbm(new DatabaseManager("data", this)),
      fileTransfer(new FileTransfer(this, control->api, this)),
      isEnding(&control->isEnding),
      syncPathsModifiedExternal(0) {
    syncFilter = QDir::AllEntries | QDir::NoSymLinks | QDir::Readable |
            QDir::Writable | QDir::NoDotAndDotDot;
}

Syncer::~Syncer() {
}

void Syncer::init() {
    fsWatcher = new QFileSystemWatcher(this);
    X_ASSERT(connect(
                 control->api, SIGNAL(signal_metadataReady(QString, QVariantMap)),
                 this, SLOT(handle_remoteMetadataReady(QString, QVariantMap)),
                 Qt::QueuedConnection));
    X_ASSERT(connect(
                 control->api, SIGNAL(signal_folderCreated(QString, QVariantMap)),
                 this, SLOT(handle_folderCreated(QString, QVariantMap)),
                 Qt::QueuedConnection));
    X_ASSERT(connect(
                 control->api, SIGNAL(signal_fileRemoved(QString, QVariantMap)),
                 this, SLOT(handle_remoteFileRemoved(QString, QVariantMap)),
                 Qt::QueuedConnection));
    X_ASSERT(connect(
                 control->api, SIGNAL(signal_deltaReset()),
                 this, SLOT(handle_remoteDeltaReset()),
                 Qt::QueuedConnection));
    X_ASSERT(connect(
                 control->api, SIGNAL(signal_deltaReady(QVariantList, bool)),
                 this, SLOT(handle_remoteDeltaReady(QVariantList, bool)),
                 Qt::QueuedConnection));
    X_ASSERT(connect(
                 fileTransfer, SIGNAL(signal_fileUploadComplete(QString, QString, QVariantMap)),
                 this, SLOT(handle_fileUploadComplete(QString, QString, QVariantMap)),
                 Qt::QueuedConnection));
    X_ASSERT(connect(
                 fileTransfer, SIGNAL(signal_downloadComplete(QString, QString, QString, QVariantMap)),
                 this, SLOT(handle_fileDownloadComplete(QString, QString, QString, QVariantMap)),
                 Qt::QueuedConnection));
    X_ASSERT(connect(
                 fileTransfer, SIGNAL(signal_localFileCreated(QString)),
                 this, SLOT(handle_localFileModifiedExternal(QString)),
                 Qt::QueuedConnection));
    X_ASSERT(connect(
                 fsWatcher, SIGNAL(directoryChanged(QString)), this,
                 SLOT(handle_localFileModifiedExternal(QString)),
                 Qt::QueuedConnection));
    verifyFs();
    broadcastSyncStatus();
}

void Syncer::verifyFs() {
    QList<PathMappingEntry> mappings = dbm->getPathMappings();
    foreach (PathMappingEntry mapping, mappings) {
        fsWatcher->addPath(mapping.localPath);
        MappedPath rootPath(mapping.id, "");
        FileStatus currentStatus = dbm->getStatus(rootPath, false);
        if (currentStatus == Unknown) {
            LocalFileInfo unverifiedLocal;
            unverifiedLocal.path = rootPath;
            unverifiedLocal.exists = true;
            dbm->setFileProperties(unverifiedLocal, FileStatus::Unverified);
            // verifyDb knows the file may exist
        } else {
            dbm->setStatus(rootPath, FileStatus::Unverified);
        }
    }
    verifyDb();
}

void Syncer::verifyDb() {
    if (*isEnding || syncPathsModifiedExternal > 0) {
        return;
    }
    // Sync files
    if (dbm->getFilesWithStatus(
                FileStatus::Syncing, true, true, MAX_SYNCING).size()
            < MAX_SYNCING) {
        foreach (MappedPath unsyncedPath, dbm->getVerifiedUnsynced(1)) {
            syncFile(unsyncedPath);
        }
    }
    // Verify local files
    if (dbm->getFilesWithStatus(
                FileStatus::Verifying, true, false, MAX_LOCAL_VERIFYING).size()
            < MAX_LOCAL_VERIFYING) {
        foreach (MappedPath unverifiedPath, dbm->getUnverified(false, 1)) {
            verifyLocal(mappedPathToAbsolute(unverifiedPath, false));
        }
    }
    // Verify remote files
    if (dbm->getFilesWithStatus(
                FileStatus::Verifying, false, true, MAX_REMOTE_VERIFYING).size()
            < MAX_REMOTE_VERIFYING) {
        foreach (MappedPath unverifiedPath, dbm->getUnverified(true, 1)) {
            verifyRemote(mappedPathToAbsolute(unverifiedPath, true));
        }
    }
}

void Syncer::verifyLocal(QString localPath) {
    if (*isEnding || syncPathsModifiedExternal > 0 ||
            !isPathSynced(localPath, false)) {
        return;
    }
    qDebug() << "verifyLocal" << localPath;
    MappedPath mappedPath = absoluteToMappedPath(localPath, false);
    FilePropertiesEntry dbProperties = dbm->getFileProperties(mappedPath);
    LocalFileInfo dbLocal = dbProperties.current_local;
    QFileInfo fileLocalInfo(localPath);
    if (fileLocalInfo.exists()) {
        fsWatcher->addPath(localPath);
    }
    LocalFileInfo updatedInfo = dbLocal;
    updatedInfo.exists = fileLocalInfo.exists();
    updatedInfo.isDir = fileLocalInfo.isDir();
    updatedInfo.lastModified = fileLocalInfo.lastModified().toMSecsSinceEpoch();
    updatedInfo.path = mappedPath;
    if (dbLocal.exists != updatedInfo.exists ||
            (dbLocal.isDir != updatedInfo.isDir) ||
            (dbLocal.lastModified != updatedInfo.lastModified)) {
        // Local file has been changed
        if (fileLocalInfo.exists() && fileLocalInfo.isFile()) {
            HashContext *hashContext = new HashContext();
            hashContext->localPath = localPath;
            hashContext->localFile = new QFile(hashContext->localPath);
            hashContext->localFile->open(QFile::ReadOnly);
            hashContext->cancel = isEnding;
            QFuture<bool> hashFuture = QtConcurrent::run(
                        &Encryption::hash, hashContext->localFile,
                        &hashContext->fileHash, hashContext->cancel);
            hashContext->futureWatcher = new QFutureWatcher<bool>();
            hashContext->futureWatcher->setFuture(hashFuture);
            X_ASSERT(connect(hashContext->futureWatcher, SIGNAL(finished()),
                             this, SLOT(handle_fileHashReady())));
            hashFutureWatcherToContext.insert(
                        reinterpret_cast<void *>(hashContext->futureWatcher), hashContext);
            dbm->setStatus(mappedPath, FileStatus::Verifying, true, false);
        } else {
            if (fileLocalInfo.isDir()) {
                QDir fileDir(fileLocalInfo.absoluteFilePath());
                foreach (QString filename, fileDir.entryList(syncFilter)) {
                    QString child = fileDir.absoluteFilePath(filename);
                    if (isPathSynced(child, false) && !*isEnding &&
                            syncPathsModifiedExternal <= 0) {
                        dbm->setStatus(absoluteToMappedPath(child, false),
                                       FileStatus::Unverified, true, false);
                    } else {
                        X_ASSERT(QMetaObject::invokeMethod(
                                     this, "verifyDb", Qt::QueuedConnection));
                        return;
                    }
                }
            }
            dbm->setFileProperties(updatedInfo, FileStatus::Verified);
        }
        // Folder has been modified, set children to be checked
        if (dbLocal.isDir) {
            dbm->setChildrenStatus(dbLocal.path, FileStatus::Unverified, true, false,
                                   false);
        }
    } else {
        if (dbProperties.current_local_status < FileStatus::Verified) {
            dbm->setFileProperties(updatedInfo, FileStatus::Verified);
        }
    }
    broadcastSyncStatus();
    X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
}
void Syncer::handle_fileHashReady() {
    void *hashFutureWatcher = reinterpret_cast<void *>(sender());
    Q_ASSERT(hashFutureWatcherToContext.contains(hashFutureWatcher));
    HashContext *context = hashFutureWatcherToContext.take(hashFutureWatcher);
    QFuture<bool> future = context->futureWatcher->future();
    if (isPathSynced(context->localPath, false)) {
        MappedPath mappedPath = absoluteToMappedPath(context->localPath, false);
        if (future.result()) {
            LocalFileInfo localInfo;
            QFileInfo fileInfo(context->localPath);
            localInfo.path = mappedPath;
            localInfo.exists = fileInfo.exists();
            localInfo.isDir = fileInfo.isDir();
            localInfo.fileHash = QString(context->fileHash.toHex());
            localInfo.lastModified = fileInfo.lastModified().toMSecsSinceEpoch();
            dbm->setFileProperties(localInfo, FileStatus::Verified);
        } else {
            qWarning() << "handle_fileHashReady" << "fail";
            dbm->setStatus(mappedPath, FileStatus::Unverified, true, false);
        }
    }
    delete context->localFile;
    delete context->futureWatcher;
    delete context;
    broadcastSyncStatus();
    X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
}

void Syncer::verifyRemote(QString remotePath) {
    MappedPath mappedPath = absoluteToMappedPath(remotePath, true);
    FilePropertiesEntry properties = dbm->getFileProperties(mappedPath);
    if (*isEnding) {
        return;
    }
    if (properties.current_remote_status != FileStatus::Unverified) {
        return;
    }
    qDebug() << "verifyRemote" << remotePath;
    dbm->setStatus(properties.current_local.path, FileStatus::Verifying, false,
                   true);
    fsWatcher->addPath(
                mappedPathToAbsolute(properties.current_local.path, false));
    X_ASSERT(QMetaObject::invokeMethod(control->api, "getMetadata",
                                       Qt::QueuedConnection,
                                       Q_ARG(QString, remotePath)));
    X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
    broadcastSyncStatus();
}

void Syncer::syncFile(MappedPath mappedPath) {
    if (*isEnding) {
        return;
    }
    qDebug() << "syncFile" << mappedPath;
    FileStatus localStatus = dbm->getStatus(mappedPath, false);
    FileStatus remoteStatus = dbm->getStatus(mappedPath, true);
    SyncAction syncAction = getSyncAction(mappedPath);
    qDebug() << "syncAction=" << syncAction;
    if (syncAction == SyncAction::NoopSynced) {
        dbm->setStatus(mappedPath, FileStatus::Synced);
    } else if (syncAction == SyncAction::Verify) {
        dbm->setStatus(mappedPath, FileStatus::Unverified);
    } else {
        if (localStatus == FileStatus::Verified &&
                remoteStatus == FileStatus::Verified) {
            dbm->setStatus(mappedPath, FileStatus::Syncing);
            QString localPath = mappedPathToAbsolute(mappedPath, false);
            QString remotePath = mappedPathToAbsolute(mappedPath, true);
            QString password = dbm->getPathMapping(mappedPath.getMapping()).password;
            if (syncAction == SyncAction::UploadFile) {
                fileTransfer->uploadFile(localPath, remotePath, password);
            } else if (syncAction == SyncAction::UploadFolder) {
                X_ASSERT(QMetaObject::invokeMethod(
                             control->api, "createFolder", Qt::QueuedConnection,
                             Q_ARG(QString, remotePath)));
            } else if (syncAction == SyncAction::DownloadFile) {
                fileTransfer->downloadFile(remotePath, localPath, password);
            } else if (syncAction == SyncAction::DownloadFolder) {
                fileTransfer->downloadFolder(localPath);
            } else if (syncAction == SyncAction::DeleteLocal) {
                QFileInfo fileInfo(localPath);
                if (fileInfo.exists()) {
                    if (fileInfo.isDir()) {
                        QDir(localPath).removeRecursively();
                    } else {
                        QFile::remove(localPath);
                    }
                }
                LocalFileInfo localInfo;
                localInfo.exists = false;
                localInfo.path = mappedPath;
                dbm->setFileProperties(localInfo, FileStatus::Synced);
            } else if (syncAction == SyncAction::DeleteRemote) {
                X_ASSERT(QMetaObject::invokeMethod(
                             control->api, "deleteFile", Qt::QueuedConnection,
                             Q_ARG(QString, remotePath)));
            }
        } else {
            dbm->setStatus(mappedPath, FileStatus::Unverified);
        }
    }
    broadcastSyncStatus();
    X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
}

Syncer::SyncAction Syncer::getSyncAction(MappedPath mappedPath) {
    FilePropertiesEntry properties = dbm->getFileProperties(mappedPath);
    if ((properties.current_local_status < FileStatus::Verified) ||
            (properties.current_remote_status < FileStatus::Verified)) {
        return SyncAction::Verify;
    }
    // Modified local and remote
    // Rename the local file (it'll be uploaded later)
    bool modifiedLocal_ =
            isModifiedLocal(properties.lastSync_local, properties.current_local);
    bool modifiedRemote_ =
            isModifiedRemote(properties.lastSync_remote, properties.current_remote);
    if ((!modifiedLocal_ && !modifiedRemote_) ||
            (!properties.current_local.exists && !properties.current_remote.exists) ||
            (properties.current_local.exists && properties.current_local.isDir &&
             properties.current_remote.exists && properties.current_remote.isDir)) {
        // Do nothing, mark synced
        return SyncAction::NoopSynced;
    } else if (modifiedLocal_ && modifiedRemote_ &&
               properties.current_local.exists &&
               properties.current_remote.exists) {
        if (properties.current_remote.isDir) {
            // Resolve local/remote conflict
            return SyncAction::DownloadFolder;
        } else {
            // Dowload the remote file. FileTransfer will take care of rename
            // if the files are different.
            return SyncAction::DownloadFile;
        }
    } else if (modifiedLocal_) {
        // Local change goes to remote
        if (properties.current_remote.exists) {
            // TODO: The remote file is sometimes being deleted when the local files
            // are already in sync.
            return SyncAction::DeleteRemote;
        } else {
            if (properties.current_local.isDir) {
                return SyncAction::UploadFolder;
            } else {
                return SyncAction::UploadFile;
            }
        }
    } else if (modifiedRemote_) {
        // Remote change applied locally
        if (properties.current_remote.exists) {
            if (properties.current_remote.isDir) {
                return SyncAction::DownloadFolder;
            } else {
                return SyncAction::DownloadFile;
            }
        } else {
            return SyncAction::DeleteLocal;
        }
    }
    return SyncAction::NoopSynced;
}

void Syncer::handle_remoteMetadataReady(
        QString remotePath,
        QVariantMap metadata) {
    if (*isEnding) {
        return;
    }
    if (isPathSynced(remotePath, true)) {
        MappedPath mappedPath = absoluteToMappedPath(remotePath, true);
        if (syncPathsModifiedExternal > 0) {
            dbm->setStatus(mappedPath, FileStatus::Unverified, false, true);
            return;
        }
        FilePropertiesEntry dbProperties = dbm->getFileProperties(mappedPath);
        RemoteFileInfo prevInfo = dbProperties.current_remote;
        RemoteFileInfo remoteInfo;
        remoteInfo.path = mappedPath;
        remoteInfo.exists = !metadata.value("is_deleted", false).toBool();
        remoteInfo.isDir = metadata.value("is_dir", false).toBool();
        remoteInfo.lastModified =
                QDateTime::fromString(metadata.value("modified").toString(),
                                      Qt::RFC2822Date).toMSecsSinceEpoch();
        remoteInfo.revision = metadata.value("rev").toString();
        if (remoteInfo.exists) {
            foreach (QVariant subdir,
                     metadata.value("contents", QStringList()).toList()) {
                QMap<QString, QVariant> subdirMetadata = subdir.toMap();
                QString subdirPath = subdirMetadata.value("path").toString();
                handle_remoteMetadataReady(subdirPath, subdirMetadata);
            }
        }
        if (isModifiedRemote(prevInfo, remoteInfo) ||
                dbProperties.current_remote_status < FileStatus::Verified) {
            dbm->setFileProperties(remoteInfo, FileStatus::Verified);
        }
        X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
    }
    broadcastSyncStatus();
}

void Syncer::handle_folderCreated(QString remotePath, QVariantMap metadata) {
    if (isPathSynced(remotePath, true)) {
        MappedPath mappedPath = absoluteToMappedPath(remotePath, true);
        if (metadata.value("conflict", false).toBool()) {
            // Create folder failed; need to go back and see what's on the server
            dbm->setStatus(mappedPath, Unverified);
        } else {
            RemoteFileInfo remoteInfo;
            remoteInfo.exists = true;
            remoteInfo.isDir = true;
            remoteInfo.lastModified =
                    QDateTime::fromString(metadata.value("modified").toString(),
                                          Qt::RFC2822Date).toMSecsSinceEpoch();
            remoteInfo.path = mappedPath;
            remoteInfo.revision = metadata.value("rev").toString();
            dbm->setFileProperties(remoteInfo, FileStatus::Synced);
        }
        X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
        broadcastSyncStatus();
    }
}

void Syncer::handle_remoteFileRemoved(
        QString remotePath,
        QVariantMap metadata) {
    MappedPath mappedPath = absoluteToMappedPath(remotePath, true);
    RemoteFileInfo remoteInfo;
    remoteInfo.exists = false;
    remoteInfo.isDir = false;
    remoteInfo.lastModified = QDateTime::fromString(
                metadata["modified"].toString(), Qt::RFC2822Date).toMSecsSinceEpoch();
    remoteInfo.path = mappedPath;
    remoteInfo.revision = metadata["rev"].toString();
    // Can't set to SYNCED immediately because remove is sometimes an
    // intermediate syncing step.
    dbm->setFileProperties(remoteInfo, Verified);
    dbm->setStatus(mappedPath, Verified);
    X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
    broadcastSyncStatus();
}

void Syncer::broadcastSyncStatus() {
    if (!*isEnding) {
        double syncedCount = dbm->getSyncedCount();
        double indexingCount = dbm->getIndexingCount();
        double syncingCount = dbm->getSyncingCount();
        emit signal_syncStatusReady(indexingCount, syncingCount, syncedCount);
    }
}

void Syncer::handle_fileUploadComplete(
        QString localPath,
        QString remotePath,
        QMap<QString, QVariant> metadata) {
    Q_UNUSED(localPath)
    MappedPath mappedPath = absoluteToMappedPath(remotePath, true);
    // Set the properties for the remote file.
    RemoteFileInfo remoteInfo;
    remoteInfo.exists = true;
    remoteInfo.isDir = false;
    remoteInfo.lastModified = QDateTime::fromString(
                metadata["modified"].toString(), Qt::RFC2822Date).toMSecsSinceEpoch();
    remoteInfo.path = mappedPath;
    remoteInfo.revision = metadata["rev"].toString();
    dbm->setFileProperties(remoteInfo, FileStatus::Synced);
    // The properties for the local file are already set.
    X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
    broadcastSyncStatus();
}

void Syncer::handle_fileDownloadComplete(
        QString remotePath,
        QString localPath,
        QString encryptionPassword,
        QVariantMap metadata) {
    qDebug() << "handle_fileDownloadComplete()" << remotePath << localPath;
    /**
    * TODO(dmiller309): Check that the password used to encrypt the file is
    * the correct one, or mark Q_UNUSED(encryptionPassword)
    */
    MappedPath mappedPath = absoluteToMappedPath(remotePath, true);
    fsWatcher->addPath(localPath);
    // Set the properties for the remote file.
    RemoteFileInfo remoteInfo;
    remoteInfo.exists = true;
    remoteInfo.isDir = false;
    remoteInfo.lastModified = QDateTime::fromString(
                metadata.value("modified").toString(), Qt::RFC2822Date)
            .toMSecsSinceEpoch();
    remoteInfo.path = mappedPath;
    remoteInfo.revision = metadata.value("rev").toString();
    dbm->setFileProperties(remoteInfo, FileStatus::Synced);
    // Set the properties for the local file.
    LocalFileInfo localInfo;
    localInfo.exists = true;
    localInfo.isDir = false;
    localInfo.fileHash = metadata["hash"].toString();
    localInfo.lastModified =
            QFileInfo(localPath).lastModified().toMSecsSinceEpoch();
    localInfo.path = mappedPath;
    dbm->setFileProperties(localInfo, FileStatus::Synced);
    X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
    broadcastSyncStatus();
}

MappedPath Syncer::nonconflictingFilepath(
        MappedPath mappedPath,
        bool isRemoteRename) {
    QString remoteLocal = (isRemoteRename ? "remote" : "local");
    QRegExp getFileNumberRegex("^([^\\.]* \\(" + remoteLocal +
                               "-)(\\d+)(\\)(?:\\..*)|(?:[^\\.]))$");
    QRegExp dirSplitRegex("^((?:.*/)?)([^/]+)$");
    QString sourceRelativePath = mappedPath.getRelativePath();
    dirSplitRegex.exactMatch(sourceRelativePath);
    QList<QString> sourceRelativePathSplit = dirSplitRegex.capturedTexts();
    QString sourceDirectory = sourceRelativePathSplit[1];
    QString sourceFilename = sourceRelativePathSplit[2];
    int startIndex = 1;
    QString leftPart, rightPart;
    if (getFileNumberRegex.exactMatch(sourceFilename)) {
        // Source filename already of form "Hello World (local-2).txt"
        QList<QString> capturedTexts = getFileNumberRegex.capturedTexts();
        Q_ASSERT(capturedTexts.size() == 4);
        startIndex = capturedTexts[2].toInt();
        leftPart = capturedTexts[1];
        rightPart = capturedTexts[3];
    } else {
        // Source filename of form "Hello World.txt"
        QRegExp splitFilenameBaseAndExtensionRegex("^([^\\.]*)(.*)");
        splitFilenameBaseAndExtensionRegex.exactMatch(sourceFilename);
        QList<QString> capturedTexts =
                splitFilenameBaseAndExtensionRegex.capturedTexts();
        Q_ASSERT(capturedTexts.size() == 3);
        leftPart = capturedTexts[1] + " (" + remoteLocal + "-";
        rightPart = ")" + capturedTexts[2];
    }
    for (int i = startIndex; true; i++) {
        QString destinationFilename = leftPart + QString::number(i) + rightPart;
        QString destinationRelativePath = sourceDirectory + destinationFilename;
        MappedPath destinationMappedPath(mappedPath.getMapping(),
                                         destinationRelativePath);
        if (!dbm->fileExists(destinationMappedPath, false) &&
                !dbm->fileExists(destinationMappedPath, true)) {
            return destinationMappedPath;
        }
    }
}

bool Syncer::isModifiedLocal(LocalFileInfo lastSync, LocalFileInfo current) {
    if (!lastSync.exists && !current.exists) {
        return false;
    }
    if (lastSync.exists != current.exists) {
        return true;
    }
    if (lastSync.isDir != current.isDir) {
        return true;
    } else if (lastSync.isDir) {
        return false;
    } else {
        return lastSync.fileHash != current.fileHash;
    }
}

bool Syncer::isModifiedRemote(RemoteFileInfo lastSync, RemoteFileInfo current) {
    if (!lastSync.exists && !current.exists) {
        return false;
    }
    if (lastSync.exists != current.exists) {
        return true;
    }
    if (lastSync.isDir != current.isDir) {
        return true;
    } else if (lastSync.isDir) {
        return false;
    } else {
        return lastSync.revision != current.revision;
    }
}

bool Syncer::pathHasParent(MappedPath mappedPath) {
    QString relativePath = mappedPath.getRelativePath();
    return (relativePath != "/") && (relativePath.contains("/"));
}

MappedPath Syncer::getParentPath(MappedPath mappedPath) {
    Q_ASSERT(pathHasParent(mappedPath));
    QString relativePath = mappedPath.getRelativePath();
    while (relativePath.endsWith("/")) {
        relativePath.chop(1);
    }
    QString parentRelativePath;
    if (!relativePath.contains("/")) {
        parentRelativePath = relativePath.left(relativePath.lastIndexOf("/"));
    }
    return MappedPath(mappedPath.getMapping(), parentRelativePath);
}

QString Syncer::mappedPathToAbsolute(MappedPath path, bool isRemote) {
    return dbm->getBasePath(path.getMapping(), isRemote) + path.getRelativePath();
}

void Syncer::handle_localFileModifiedExternal(QString localPath) {
    if (isPathSynced(localPath, false)) {
        MappedPath mappedPath = absoluteToMappedPath(localPath, false);
        dbm->setStatus(mappedPath, FileStatus::Unverified, true, false);
        X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
    } else {
        fsWatcher->removePath(localPath);
    }
}

void Syncer::handle_remoteDeltaReady(QVariantList metadataList, bool hasMore) {
    foreach (QVariant metadata_variant, metadataList) {
        if (*isEnding) {
            return;
        }
        QVariantMap metadata = metadata_variant.toMap();
        QString remotePath = metadata["path"].toString();
        if (isPathSynced(remotePath, true)) {
            handle_remoteMetadataReady(remotePath, metadata);
        }
    }
    if (!hasMore) {
        hasFirstDelta = true;
    }
    verifyDb();
}

void Syncer::handle_remoteDeltaReset() {
    dbm->invalidateAllRemote();
    X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
    broadcastSyncStatus();
}

QList<PathMappingEntry> Syncer::getDefaultPathMappings() {
    QMap<QStandardPaths::StandardLocation, QString> pathMap;
    pathMap[QStandardPaths::DesktopLocation] = "/Desktop";
    pathMap[QStandardPaths::DocumentsLocation] = "/Documents";
    pathMap[QStandardPaths::MusicLocation] = "/Music";
    pathMap[QStandardPaths::PicturesLocation] = "/Photos";
    pathMap[QStandardPaths::MoviesLocation] = "/Videos";
    QList<PathMappingEntry> pathMappings;
    for (QMapIterator<QStandardPaths::StandardLocation, QString> it(pathMap);
         it.hasNext();) {
        it.next();
        QString localPath = QStandardPaths::writableLocation(it.key());
        QString remotePath = it.value();
        pathMappings.append(PathMappingEntry(0, localPath, remotePath, ""));
    }
    return pathMappings;
}

int Syncer::addSyncPathMapping(
        QString localPath,
        QString remotePath,
        QString password) {
    QList<PathMappingEntry> dbMappingEntries = dbm->getPathMappings();
    foreach (PathMappingEntry dbMappingEntry, dbMappingEntries) {
        if (localPath == dbMappingEntry.localPath &&
                remotePath == dbMappingEntry.remotePath &&
                password == dbMappingEntry.password) {
            return 0;
        }
        if (directoryIsOrContainsFile(localPath, dbMappingEntry.localPath) ||
                directoryIsOrContainsFile(dbMappingEntry.localPath, localPath) ||
                directoryIsOrContainsFile(remotePath, dbMappingEntry.remotePath) ||
                directoryIsOrContainsFile(dbMappingEntry.remotePath, remotePath)) {
            dbm->removePathMapping(dbMappingEntry.id);
        }
    }
    int mappingId = dbm->addPathMapping(localPath, remotePath, password);
    MappedPath rootMappedPath(mappingId, "");
    dbm->setStatus(rootMappedPath, FileStatus::Unverified);
    emit signal_syncPathsChanged(dbm->getPathMappings());
    syncPathsModifiedExternal--;
    X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
    broadcastSyncStatus();
}

int Syncer::removeSyncPathMappingById(int mappingId) {
    int mappingsChanged = dbm->removePathMapping(mappingId);
    if (mappingsChanged > 0) {
        emit signal_syncPathsChanged(dbm->getPathMappings());
        broadcastSyncStatus();
    }
    syncPathsModifiedExternal--;
    X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
    return mappingsChanged;
}
int Syncer::removeSyncPathMapping(QString localPath, QString remotePath) {
    QList<PathMappingEntry> dbPathMappingEntries = dbm->getPathMappings();
    bool mappingsChanged = false;
    foreach (PathMappingEntry dbPathMappingEntry, dbPathMappingEntries) {
        if (directoryIsOrContainsFile(localPath, dbPathMappingEntry.localPath) &&
                directoryIsOrContainsFile(remotePath, dbPathMappingEntry.remotePath)) {
            dbm->removePathMapping(dbPathMappingEntry.id);
            mappingsChanged = true;
        }
    }
    if (mappingsChanged) {
        emit signal_syncPathsChanged(dbm->getPathMappings());
        broadcastSyncStatus();
    }
    syncPathsModifiedExternal--;
    X_ASSERT(QMetaObject::invokeMethod(this, "verifyDb", Qt::QueuedConnection));
    return mappingsChanged;
}

bool Syncer::directoryIsOrContainsFile(QString directory, QString file) {
    directory.replace(QRegExp("/+$"), "");
    file.replace(QRegExp("/+$"), "");
    return (file == directory) || file.startsWith(directory + "/");
}

bool Syncer::isPathSynced(QString absolutePath, bool isRemote) {
    absolutePath = absolutePath.toLower();
    foreach (PathMappingEntry dbPathMappingEntry, dbm->getPathMappings()) {
        QString syncedPath = (isRemote ? dbPathMappingEntry.remotePath
                                       : dbPathMappingEntry.localPath);
        syncedPath = syncedPath.toLower();
        while (syncedPath.endsWith("/")) {
            syncedPath.chop(1);
        }
        if (absolutePath == syncedPath) {
            return true;
        }
        if (absolutePath.startsWith(syncedPath + "/")) {
            return true;
        }
    }
    return false;
}

MappedPath Syncer::absoluteToMappedPath(QString absolutePath, bool isRemote) {
    // Determine which mapping absolutePath belongs to.
    // Get the path relative to the mapping.
    Q_ASSERT(isPathSynced(absolutePath, isRemote));
    MappedPath mappedPath;
    foreach (PathMappingEntry dbPathMapping, dbm->getPathMappings()) {
        QString syncedPath =
                (isRemote ? dbPathMapping.remotePath : dbPathMapping.localPath);
        while (syncedPath.endsWith("/")) {
            syncedPath.chop(1);
        }
        if (absolutePath.toLower().startsWith(syncedPath.toLower())) {
            mappedPath.first = dbPathMapping.id;
            mappedPath.second =
                    absolutePath.right(absolutePath.length() - syncedPath.length())
                    .replace(QRegExp("/+$"), "");
            break;
        }
    }
    return mappedPath;
}

}  // namespace mfsync
}  // namespace com
