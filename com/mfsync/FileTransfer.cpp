/*
 * Copyright 2014 Andrew William Miller
 */

#include "com/mfsync/FileTransfer.h"

#include <util/FolderUtil.h>

#include <QtCore/QtGlobal>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtConcurrent/QtConcurrent>
#include <QtConcurrent/QtConcurrentRun>

#include "com/mfsync/Encryption.h"
#include "com/mfsync/Syncer.h"

namespace com {
namespace mfsync {

FileTransfer::FileTransfer(
        Syncer *syncer,
        DropboxAPI *api,
        QObject *parent) :
    QObject(parent),
    syncer(syncer),
    api(api) {
    X_ASSERT(connect(api, SIGNAL(signal_fileDownloadComplete(QString, QFile*, QMap<QString,QVariant>)),
                     this, SLOT(handle_fileDownloaded(QString, QFile*, QMap<QString,QVariant>)),
                     Qt::QueuedConnection));
    X_ASSERT(connect(api, SIGNAL(signal_fileUploadComplete(QFile*, QString, QVariantMap)),
                     this, SLOT(handle_fileUploadComplete(QFile*, QString, QVariantMap)),
                     Qt::QueuedConnection));
}

void FileTransfer::downloadFile(
        QString remotePath,
        QString localPath,
        QString encryptionPassword) {
    QString templateName = QFileInfo(localPath).fileName()+"-enc";
    DownloadContext *context = new DownloadContext();
    context->remotePath = remotePath;
    context->localPath = localPath;
    context->encryptionPassword = encryptionPassword;
    context->encryptedFile = new QTemporaryFile(templateName);
    context->encryptedFile->open(QFile::ReadWrite);
    downloadTempFileToContext[context->encryptedFile] = context;
    X_ASSERT(QMetaObject::invokeMethod(
                 api, "downloadFile", Qt::QueuedConnection,
                 Q_ARG(QString, remotePath),
                 Q_ARG(QFile*, context->encryptedFile)));
}
void FileTransfer::handle_fileDownloaded(
        QString remotePath,
        QFile *downloadTempFile,
        QVariantMap metadata) {
    QString templateName = QFileInfo(*downloadTempFile).fileName()+"-dec";
    DownloadContext *context = downloadTempFileToContext.take(downloadTempFile);
    Q_ASSERT(remotePath == context->remotePath);
    context->metadata = metadata;
    X_ASSERT(context->encryptedFile->reset());
    context->decryptedFile = new QTemporaryFile(templateName);
    X_ASSERT(context->decryptedFile->open());
    context->watcher = new QFutureWatcher<bool>();
    context->future = QtConcurrent::run(
                &Encryption::decrypt, context->encryptedFile,
                context->decryptedFile, context->encryptionPassword,
                syncer->isEnding);
    context->watcher->setFuture(context->future);
    downloadDecryptWatcherToContext[context->watcher] = context;
    X_ASSERT(connect(context->watcher, SIGNAL(finished()),
                     this, SLOT(handle_downloadDecryptionFinished())));
}
void FileTransfer::handle_downloadDecryptionFinished() {
    void *decryptWatcher = reinterpret_cast<void*>(sender());
    DownloadContext *context =
            downloadDecryptWatcherToContext.take(decryptWatcher);
    X_ASSERT(context->future.result());
    delete context->encryptedFile;
    delete context->watcher;
    X_ASSERT(context->decryptedFile->reset());
    context->fileHash = new QByteArray();
    context->future = QtConcurrent::run(
                &Encryption::hash, context->decryptedFile, context->fileHash,
                syncer->isEnding);
    context->watcher = new QFutureWatcher<bool>();
    context->watcher->setFuture(context->future);
    downloadHashWatcherToContext[context->watcher] = context;
    X_ASSERT(connect(context->watcher, SIGNAL(finished()),
                     this, SLOT(handle_downloadHashFinished())));
}
void FileTransfer::handle_downloadHashFinished() {
    void *hashWatcher = reinterpret_cast<void*>(sender());
    DownloadContext *context = downloadHashWatcherToContext.take(hashWatcher);
    X_ASSERT(context->future.result());
    context->decryptedFile->close();
    if (!context->future.result()) {
        qWarning() << "File hash failed:" << context->localPath;
        // TODO(dmiller309): Check the result in the QFuture
    }
    delete context->watcher;
    bool fileSame = false;
    QString renamedPath;
    context->metadata["hash"] = QString(context->fileHash->toHex());
    QFileInfo fsInfo = QFileInfo(context->localPath);
    if (fsInfo.exists()) {
        MappedPath mappedPath =
                syncer->absoluteToMappedPath(context->localPath, false);
        FilePropertiesEntry dbProperties =
                syncer->dbm->getFileProperties(mappedPath);
        if (dbProperties.current_local.fileHash ==
                context->metadata["hash"].toString() &&
                dbProperties.current_local.lastModified ==
                fsInfo.lastModified().toMSecsSinceEpoch()) {
            /**
             * TODO(dmiller309): The file could have been modified locally
             * but still have the same hash, in which case the files would
             * still be the same but the file on disk would have to be hashed
             * to know for sure.
             */
            fileSame = true;
        } else {
            renamedPath = syncer->mappedPathToAbsolute(
                        syncer->nonconflictingFilepath(mappedPath, false),
                        false);
            X_ASSERT(QFile::rename(context->localPath, renamedPath));
        }
    }
    QDir::root().mkpath(QFileInfo(context->localPath).path());
    if (!fileSame) {
        if (context->decryptedFile->rename(context->localPath)) {
            context->decryptedFile->setAutoRemove(false);
        } else {
            qWarning() << "Failed to move downloaded file out of cache.";
        }
    }
    delete context->decryptedFile;
    emit signal_downloadComplete(
                context->remotePath, context->localPath,
                context->encryptionPassword, context->metadata);
    if (!fileSame) {
        emit signal_localFileCreated(renamedPath);
    }
    delete context;
}

void FileTransfer::uploadFile(
        QString localPath,
        QString remotePath,
        QString encryptionPassword) {
    UploadContext *context = new UploadContext();
    context->localPath = localPath;
    context->remotePath = remotePath;
    context->encryptionPassword = encryptionPassword;
    context->localFile = new QFile(localPath);
    context->localFile->open(QFile::ReadOnly);
    QString templateName = QFileInfo(localPath).fileName()+"-up-dec";
    context->encryptedFile = new QTemporaryFile(templateName);
    context->encryptedFile->open();
    context->future = QtConcurrent::run(
                &Encryption::encrypt, context->localFile,
                context->encryptedFile, context->encryptionPassword,
                syncer->isEnding);
    context->watcher = new QFutureWatcher<bool>();
    context->watcher->setFuture(context->future);
    X_ASSERT(connect(context->watcher, SIGNAL(finished()),
                     this, SLOT(handle_uploadEncryptionFinished())));
    uploadEncryptionWatcherToContext[context->watcher] = context;
}
void FileTransfer::handle_uploadEncryptionFinished() {
    void *encryptWatcher = reinterpret_cast<void*>(sender());
    UploadContext *context =
            uploadEncryptionWatcherToContext.take(encryptWatcher);
    X_ASSERT(context->future.result());
    delete context->watcher;
    context->encryptedFile->reset();
    Q_ASSERT(context->future.result());
    X_ASSERT(QMetaObject::invokeMethod(
                 api, "uploadFile", Qt::QueuedConnection,
                 Q_ARG(QFile*, context->encryptedFile),
                 Q_ARG(QString, context->remotePath)));
    uploadLocalTempToContext[context->encryptedFile] = context;
}
void FileTransfer::handle_fileUploadComplete(
        QFile *localFile,
        QString remotePath,
        QVariantMap metadata) {
    UploadContext *context = uploadLocalTempToContext.take(localFile);
    context->metadata = metadata;
    Q_ASSERT(remotePath == context->remotePath);
    delete context->encryptedFile;
    delete context->localFile;
    emit signal_fileUploadComplete(context->localPath,
                               context->remotePath,
                               context->metadata);
    delete context;
}

void FileTransfer::downloadFolder(
        QString localPath) {
    QFileInfo localInfo(localPath);
    if (!localInfo.absoluteDir().exists()) {
        downloadFolder(localInfo.absolutePath());
    }
    if (localInfo.exists() && !localInfo.isDir()) {
        // There's a local conflict
        MappedPath mappedPath =
                syncer->absoluteToMappedPath(localPath, false);
        MappedPath renamedMappedPath =
                syncer->nonconflictingFilepath(mappedPath, false);
        QString renamedPath =
                syncer->mappedPathToAbsolute(renamedMappedPath, false);
        X_ASSERT(QFile::rename(localPath, renamedPath));
        emit signal_localFileCreated(renamedPath);
    }
    localInfo.dir().mkdir(localInfo.fileName());
    Q_ASSERT(QFileInfo(localPath).exists());
    emit signal_localFileCreated(localPath);
}

}  // namespace mfsync
}  // namespace com
